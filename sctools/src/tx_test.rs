use anyhow::Context;

use log::{error, info};
use loragw_hw::lib::*;
use std::process::ExitCode;
use std::{fs::File, ops::Deref, time::Duration};

use crate::TxArgs;

pub async fn run(args: TxArgs) -> anyhow::Result<ExitCode> {
    let cfg_file = File::open(args.config.deref())?;
    let reader = json_comments::StripComments::new(cfg_file);
    let root_config: cfg::RootConfig = serde_json::from_reader(reader).with_context(|| {
        format!(
            "error parsing config file: {}",
            args.config.as_path().display()
        )
    })?;

    if let Some(reset_pin) = root_config.board.reset_pin {
        if let Err(e) = sctools::reset_card(reset_pin) {
            error!("{}", e);
        }
    }

    info!("waiting for micro to boot...");
    std::thread::sleep(Duration::from_secs(20));

    let mut sc: loragw_hw::secure_concentrator::SecureConcentrator = match root_config
        .board
        .com_type
    {
        ComType::SPI_PROXY => loragw_hw::secure_concentrator::with_spi(&root_config.board.com_path)?,
    };

    sc.run(&root_config.board).await?;

    sc.connect(&root_config.board).await?;

    for i in 0..args.num_packets {
        let payload = bytes::BytesMut::new();

        let pkt = TxPkt {
            freq_hz: (args.frequency * 1_000_000_f32) as u32,
            rf_chain: 0,
            rf_power: args.power,
            datarate: args.datarate,
            coderate: CodingRate::CR_4_5,
            invert_pol: false,
            preamble: None,
            no_crc: false,
            no_header: false,
            payload: payload.freeze(),
            tx_mode: TxMode::Immediate,
        };

        println!(
            "Sending pkt#{} {}MHz {} {}dBm",
            i, args.frequency, pkt.datarate, pkt.rf_power
        );

        match sc.send(&root_config.board, pkt).await {
            Ok(()) => println!("sent"),
            Err(e) => error!("{}", e),
        }

        tokio::time::sleep(Duration::from_millis(args.delay as u64)).await;
    }

    Ok(ExitCode::SUCCESS)
}
