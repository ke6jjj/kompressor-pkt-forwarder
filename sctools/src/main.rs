use std::{path::PathBuf, process::ExitCode, str::FromStr};

use clap::{Args, Parser, Subcommand};
use loragw_hw::lib::Datarate;

mod download;
mod rx_test;
mod tx_test;

fn init_logging() {
    //use simple_logger::SimpleLogger;
    //SimpleLogger::new().init().unwrap();

    env_logger::init();
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Receive test
    Rx {
        #[arg(short, long, value_name = "config file")]
        config: PathBuf,
    },

    /// Send test
    Tx(TxArgs),

    /// Download firmware
    Download(DownloadArgs),
}

#[derive(Debug, Args)]
pub struct TxArgs {
    #[arg(short, long, value_name = "config file")]
    config: PathBuf,

    /// Radio Tx frequency in MHz
    #[arg(short = 'f', long, value_name = "frequency")]
    frequency: f32,

    /// Datarate (I.E. SF7BW125, SF10BW125, etc)
    #[arg(short, long, value_name = "data rate", value_parser = parse_datarate, default_value = "SF7BW125")]
    datarate: Datarate,

    /// Number of packets to send
    #[arg(short = 'n', long, value_name = "uint", default_value_t = 1)]
    num_packets: usize,

    /// RF power in dBm
    #[arg(short = 'p', long, value_name = "int", default_value_t = 10)]
    power: i8,

    /// Delay between sending packets
    #[arg(short = 'w', long, value_name = "ms", default_value_t = 500)]
    delay: u32,
}

#[derive(Debug, Args)]
pub struct DownloadArgs {
    /// The firmware file
    #[arg(short, long, value_name = "firmware.bin")]
    firmware: PathBuf,

    /// serial port
    #[arg(short, long, value_name = "serial port")]
    serial_port: String,

    /// reset GPIO pin.
    #[arg(short, long, value_name = "reset pin")]
    reset_pin: u32,
}

fn parse_datarate(v: &str) -> Result<Datarate, String> {
    Datarate::from_str(v).map_err(|e| format!("can not parse datarate: {} {}", v, e))
}

#[tokio::main]
async fn main() -> anyhow::Result<ExitCode> {
    init_logging();

    let cli = Cli::parse();
    match cli.command {
        Commands::Rx { config } => {
            return rx_test::run(config).await;
        }

        Commands::Tx(args) => {
            return tx_test::run(args).await;
        }

        Commands::Download(args) => {
            return download::run(args).await;
        }
    }
}
