use std::{fs::File, io::prelude::*, process::ExitCode, time::Duration};

use log::error;

use crate::DownloadArgs;

pub async fn run(args: DownloadArgs) -> anyhow::Result<ExitCode> {
    let mut serial = serialport::new(args.serial_port, 38400)
        .timeout(Duration::from_millis(1000))
        .open()?;

    let mut firmware_file = File::open(args.firmware)?;

    println!("reset");
    sctools::reset_card(args.reset_pin)?;

    tokio::time::sleep(Duration::from_millis(1000)).await;

    println!("send download command");
    // send a key to get the bootloader into download mode
    serial.write_all(&[13])?;
    serial.flush()?;

    tokio::time::sleep(Duration::from_millis(7000)).await;
    serial.clear(serialport::ClearBuffer::All)?;

    tokio::task::spawn_blocking(move || {
        println!("start xmodem transfer");
        let mut transfer = xmodem::Xmodem::new();
        match transfer.send(&mut serial, &mut firmware_file) {
            Ok(()) => {
                println!("download success");
                Ok(ExitCode::SUCCESS)
            }

            Err(e) => {
                error!("xmodem transfer error: {:?}", e);
                Ok(ExitCode::FAILURE)
            }
        }
    })
    .await?
}
