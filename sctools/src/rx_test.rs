use anyhow::Context;
use base64::prelude::*;
use chrono::SecondsFormat;
use futures::{prelude::*, FutureExt};
use log::{error, info};
use loragw_hw::lib::*;
use std::time::Duration;
use std::{collections::VecDeque, process::ExitCode};
use std::{fs::File, ops::Deref, path::PathBuf};
use tokio::signal::unix::{signal, SignalKind};

pub async fn run(config: PathBuf) -> anyhow::Result<ExitCode> {
    let cfg_file = File::open(config.deref())?;
    let reader = json_comments::StripComments::new(cfg_file);
    let root_config: cfg::RootConfig = serde_json::from_reader(reader)
        .with_context(|| format!("error parsing config file: {}", config.as_path().display()))?;

    if let Some(reset_pin) = root_config.board.reset_pin {
        if let Err(e) = sctools::reset_card(reset_pin) {
            error!("{}", e);
        }
    }

    info!("waiting for micro to boot...");
    std::thread::sleep(Duration::from_secs(10));

    let mut sc: loragw_hw::secure_concentrator::SecureConcentrator = match root_config
        .board
        .com_type
    {
        ComType::SPI_PROXY => loragw_hw::secure_concentrator::with_spi(&root_config.board.com_path)?,
    };

    sc.run(&root_config.board).await?;

    sc.connect(&root_config.board).await?;
    let sc_pubkey = sc.sc_pubkey().unwrap();

    let mut rx_stream = sc.get_rx_stream();

    let mut sigterm = signal(SignalKind::terminate())?;
    let mut sigint = signal(SignalKind::interrupt())?;

    const PKT_QUEUE_MAX_SIZE: usize = 10;
    let mut last_few_pkts = VecDeque::with_capacity(PKT_QUEUE_MAX_SIZE);

    loop {
        let mut next_rx_pkt = rx_stream.next().fuse();
        let term_sig = sigterm.recv().fuse();
        futures::pin_mut!(term_sig);
        let int_sig = sigint.recv().fuse();
        futures::pin_mut!(int_sig);

        futures::select! {
            pkt = next_rx_pkt => {
                let rx_time = chrono::offset::Utc::now();
                match pkt.unwrap() {
                    nlighten::types::SCStreamMsg::RxPkt(pkt) => {
                        while last_few_pkts.len() >= PKT_QUEUE_MAX_SIZE {
                            last_few_pkts.pop_back();
                        }
                        last_few_pkts.push_front(pkt.clone());

                        let gps_time = match pkt.pkt.gps_time {
                            Some(gps_time) => gps_time.as_datetime().to_string(),
                            None => String::from("none"),
                        };

                        let gps_pos = match pkt.pkt.pos {
                            Some(gps_pos) => gps_pos.to_string(),
                            None => String::from("none"),
                        };
                        println!("----- {} packet: {:04x} -----", rx_time.to_rfc3339_opts(SecondsFormat::Micros, false), pkt.id);
                        println!("  freq: {}", pkt.pkt.freq);
                        println!("  datr: {}", pkt.pkt.datarate);
                        println!("  snr : {}", pkt.pkt.snr);
                        println!("  tmst: {}", pkt.pkt.tmst);
                        println!("  rssi_sig (dBm): {}", pkt.pkt.rssi);
                        println!("  rssi_chn (raw): {}", pkt.rssic);
                        println!("  gps time: {}", gps_time);
                        println!("  gps_pos : {}", gps_pos);
                        println!("  data: {}", BASE64_STANDARD_NO_PAD.encode(pkt.pkt.payload));
                        println!("");
                    }

                    nlighten::types::SCStreamMsg::RxPktSig(pkt) => {

                        let orig_pkt = last_few_pkts.iter().find(|x| x.id == pkt.id);
                        let verify = match orig_pkt {
                            Some(orig_pkt) => format!("{}", orig_pkt.pkt.verify(&sc_pubkey, &pkt.sig)),
                            None => String::from("UNKNOWN"),
                        };

                        println!("----- {} sig packet: {:04x} -----", rx_time.to_rfc3339_opts(SecondsFormat::Micros, false), pkt.id);
                        println!("  sig: {}", BASE64_STANDARD_NO_PAD.encode(pkt.sig));
                        println!("  verify: {}", verify);
                        println!("");
                    }

                    msg => {
                        warn!("unhandled type: {:?}", msg);
                    }
                }

            }

            _ = term_sig => {
                info!("exit due to signal");
                return Ok(ExitCode::SUCCESS)
            }

            _ = int_sig => {
                info!("exit due to signal");
                return Ok(ExitCode::SUCCESS)
            }


        }
    }
}
