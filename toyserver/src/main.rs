use anyhow::Result;
use bytes::{BufMut, BytesMut};
use tokio::{
    net::UdpSocket,
    sync::mpsc::{UnboundedReceiver, UnboundedSender},
};

use log::{error, info, warn};

fn parse(pkt: &[u8]) -> Result<serde_json::Value, serde_json::Error> {
    let _proto_version = pkt[0];
    let _token = u16::from_be_bytes([pkt[1], pkt[2]]);
    let _type_id = u8::from_be_bytes([pkt[3]]);
    let _gateway_id = u64::from_be_bytes([
        pkt[4], pkt[5], pkt[6], pkt[7], pkt[8], pkt[9], pkt[10], pkt[11],
    ]);

    serde_json::from_slice(&pkt[12..])
}

fn init_logging() {
    use simple_logger::SimpleLogger;
    SimpleLogger::new().init().unwrap();
}

fn read_keyboard(tx: UnboundedSender<u8>) {
    let getch = getch::Getch::new();
    loop {
        if let Ok(ch) = getch.getch() {
            println!("key press: {}", ch);
            if let Err(e) = tx.send(ch) {
                error!("{}", e);
            }
        }
    }
}

async fn send_packet(socket: &UdpSocket) -> anyhow::Result<()> {
    info!("sending packet");
    let json_str = r#"
    {
        "txpk": {
            "imme": true,
            "freq": 920.6,
            "rfch": 0,
            "powe": 14,
            "modu": "",
            "datr": "SF12BW500",
            "codr": "4/5",
            "size": 11,
            "data": "aGVsbG8gd29ybGQ="
        }
    }
    "#;
    let mut buf = BytesMut::new();
    buf.put_u8(2);
    buf.put_u16(2342);
    buf.put_u8(0x03);
    buf.put_slice(json_str.as_bytes());

    let _ = socket.send_to(&buf, "127.0.0.1:9000").await?;

    Ok(())
}

async fn handle_keyboard(rx: &mut UnboundedReceiver<u8>, socket: &UdpSocket) {
    match rx.recv().await {
        Some(k) => match k {
            115 => {
                let _ = send_packet(socket).await;
            }

            k => {
                warn!("unsupported key: {}", k);
            }
        },

        None => {}
    }
}

async fn rx_pkt(socket: &UdpSocket, mut buf: &mut [u8]) -> anyhow::Result<()> {
    let (len, _addr) = socket.recv_from(&mut buf).await?;
    //info!("received: {} from {}", len, addr);
    if let Ok(pkt) = parse(&buf[..len]) {
        info!("RX:\n{}\n\n", serde_json::to_string_pretty(&pkt)?);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    init_logging();

    let bind_addr = "0.0.0.0:1680";
    info!("UDP bind: {}", &bind_addr);
    let socket = UdpSocket::bind(&bind_addr).await?;
    let mut buf = [0_u8; 50000];

    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel();

    tokio::task::spawn_blocking(move || {
        read_keyboard(tx);
    });

    //tokio::spawn(rx_keyboard(rx, &socket));

    loop {
        tokio::select! {

            _ = rx_pkt(&socket, &mut buf) => {

            },

            _ = handle_keyboard(&mut rx, &socket) => {

            }
        }
    }
}
