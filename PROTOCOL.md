
Basic communication protocol between LoRa gateway and Network Server
====================================================================


## 1. Introduction

The protocol between the gateway and the server is purposefully very basic and
for demonstration purpose only, or for use on private and reliable networks.

There is no authentication of the gateway or the server, and the acknowledges
are only used for network quality assessment, not to correct UDP datagrams
losses (no retries).


## 2. System schematic and definitions

	 ((( Y )))
	     |
	     |
	+ - -|- - - - - - - - - - - - - +        xxxxxxxxxxxx          +--------+
	| +--+-----------+     +------+ |       xx x  x     xxx        |        |
	| |    Secure    |     |      | |      xx  Internet  xx        |        |
	| | Concentrator |<--->| Host |<-------xx     or    xx-------->|        |
	| |              | SPI |      | |      xx  Intranet  xx        | Server |
	| +--------------+     +------+ |       xxxx   x   xxxx        |        |
	|                               |           xxxxxxxx           |        |
	|                               |                              |        |
	|                               |                              +--------+
	|                               |
	|             Gateway           |
	+- - - - - - - - - - - - - - - -+

__Concentrator__: radio RX/TX board, based on Semtech multichannel modems
(SX130x), transceivers (SX125x) and/or low-power stand-alone modems (SX127x).

__Host__: embedded computer on which the packet forwarder is run. Drives the
concentrator through a SPI link.

__Gateway__: a device composed of at least one radio concentrator, a host, some
network connection to the internet or a private network (Ethernet, 3G, Wifi,
microwave link), and optionally a GPS receiver for synchronization.

__Server__: an abstract computer that will process the RF packets received and
forwarded by the gateway, and issue RF packets in response that the gateway
will have to emit.

It is assumed that the gateway can be behind a NAT or a firewall stopping any
incoming connection.
It is assumed that the server has an static IP address (or an address solvable
through a DNS service) and is able to receive incoming connections on a
specific port.


## 3. Upstream protocol

### 3.1. Sequence diagram ###

	+---------+                                                    +---------+
	| Gateway |                                                    | Server  |
	+---------+                                                    +---------+
	     | -----------------------------------\                         |
	     |-| When 1-N RF packets are received |                         |
	     | ------------------------------------                         |
	     |                                                              |
	     | PUSH_DATA (token X, GW MAC, JSON payload)                    |
	     |------------------------------------------------------------->|
		 |                                                              |
         | PUSH_DATA_SIG (token X, GW MAC, JSON payload)                |
	     |------------------------------------------------------------->|
	     |                                                              |
	     |                                           PUSH_ACK (token X) |
	     |<-------------------------------------------------------------|
	     |                              ------------------------------\ |
	     |                              | process packets *after* ack |-|
	     |                              ------------------------------- |
	     |                                                              |

### 3.2. PUSH_DATA packet ###

That packet type is used by the gateway mainly to forward the RF packets
received, and associated metadata, to the server.

 Bytes  | Function
:------:|---------------------------------------------------------------------
 0      | protocol version = 3
 1-2    | random token
 3      | PUSH_DATA identifier 0x00
 4-11   | Gateway unique identifier (MAC address)
 12-end | JSON object, starting with {, ending with }, see section 4

### 3.3 PUSH_DATA_SIG ###
The packet type contains a EC25519 signature of the RX data contained in
PUSH_DATA packets.

 Bytes  | Function
:------:|---------------------------------------------------------------------
 0      | protocol version = 3
 1-2    | random token
 3      | PUSH_DATA_SIG identifier 0x06
 4-11   | Gateway unique identifier (MAC address)
 12-end | JSON object, starting with {, ending with }, see section 4.1

### 3.4. PUSH_ACK packet ###

That packet type is used by the server to acknowledge immediately all the
PUSH_DATA packets received.

 Bytes  | Function
:------:|---------------------------------------------------------------------
 0      | protocol version = 3
 1-2    | same token as the PUSH_DATA packet to acknowledge
 3      | PUSH_ACK identifier 0x01


## 4. Upstream JSON data structure

The root object contains a single rx "pkt" and a "key"

``` json
{
	"key": 2397234,
	"pkt": {
		"pos": {
			"lat": 425436334,
			"lon": -713926498,
			"hacc": 6269,
			"vacc": null,
			"height": 78703
		},
		"snr": -225,
		"freq": 905299804,
		"rssi": 119,
		"tmst": 880658660,
		"datarate": "SF7BW125",
		"gatwy_id": 6403564294490687,
		"time_utc": "2022-05-04T03:48:58.592567Z",
		"payload": "aGVsbG8gd29ybGQ="
	}
}
```

key - the key is a unique u32 identifier of the packet.
pkt - object that contains RF packet and associated metadata

 Name    |  Type  | Function
:-------:|:------:|--------------------------------------------------------------
 lat     | number | degrees latitude scale 1e-7
 lon     | number | degrees longitude scale 1e-7
 hacc    | number | Horizontal accuracy estimate (unit: mm)
 vacc    | number | Vertical accuracy estimate (unit: mm)
height   | number | Height above ellipsoid (unit: mm)
 snr     | number | Signal-to-noise radio (unit 0.1 dB)
 freq    | number | frequency (unit hertz)
 rssi    | number | RSSI of the channel in dBm (signed integer, 1 dB precision)
 tmst    | number | Internal timestamp of "RX finished" event (32b unsigned)
payload  | string | Base64 encoded RF packet payload, padded
datarate | string | LoRa datarate identifier (eg. SF12BW500)
gatwy_id | number | u64 globally unique gateway id
time_utc | string | UTC time of pkt RX, us precision, ISO 8601 'compact' format


## 4.1 PUSH_DATA_SIG JSON data structure

```json
{
	"key": 2397234,
	"sig": "NWU3ZDVnNnM1ZjVlN2Q1ZzZzNWY1ZTdkNWc2czVmNWU3ZDVnNnM1ZjVlN2Q1ZzZzNWY1ZTdkNWc2czVmNTU1NQ=="
}
```

 Name    |  Type  | Function
:-------:|:------:|--------------------------------------------------------------
 key     | number | the key is a unique u32 identifier of the packet.
 sig     | string | Base64 encoded EC25519 signature of the rf packet


## 5. Downstream protocol

### 5.1. Sequence diagram ###

	+---------+                                                    +---------+
	| Gateway |                                                    | Server  |
	+---------+                                                    +---------+
	     | -----------------------------------\                         |
	     |-| Every N seconds (keepalive time) |                         |
	     | ------------------------------------                         |
	     |                                                              |
	     | PULL_DATA (token Y, MAC@)                                    |
	     |------------------------------------------------------------->|
	     |                                                              |
	     |                                           PULL_ACK (token Y) |
	     |<-------------------------------------------------------------|
	     |                                                              |

	+---------+                                                    +---------+
	| Gateway |                                                    | Server  |
	+---------+                                                    +---------+
	     |      ------------------------------------------------------\ |
	     |      | Anytime after first PULL_DATA for each packet to TX |-|
	     |      ------------------------------------------------------- |
	     |                                                              |
	     |                            PULL_RESP (token Z, JSON payload) |
	     |<-------------------------------------------------------------|
	     |                                                              |
	     | TX_ACK (token Z, JSON payload)                               |
	     |------------------------------------------------------------->|

### 5.2. PULL_DATA packet ###

That packet type is used by the gateway to poll data from the server.

This data exchange is initialized by the gateway because it might be
impossible for the server to send packets to the gateway if the gateway is
behind a NAT.

When the gateway initialize the exchange, the network route towards the
server will open and will allow for packets to flow both directions.
The gateway must periodically send PULL_DATA packets to be sure the network
route stays open for the server to be used at any time.

 Bytes  | Function
:------:|---------------------------------------------------------------------
 0      | protocol version = 2
 1-2    | random token
 3      | PULL_DATA identifier 0x02
 4-11   | Gateway unique identifier (MAC address)

### 5.3. PULL_ACK packet ###

That packet type is used by the server to confirm that the network route is
open and that the server can send PULL_RESP packets at any time.

 Bytes  | Function
:------:|---------------------------------------------------------------------
 0      | protocol version = 2
 1-2    | same token as the PULL_DATA packet to acknowledge
 3      | PULL_ACK identifier 0x04

### 5.4. PULL_RESP packet ###

That packet type is used by the server to send RF packets and associated
metadata that will have to be emitted by the gateway.

 Bytes  | Function
:------:|---------------------------------------------------------------------
 0      | protocol version = 2
 1-2    | random token
 3      | PULL_RESP identifier 0x03
 4-end  | JSON object, starting with {, ending with }, see section 6

### 5.5. TX_ACK packet ###

That packet type is used by the gateway to send a feedback to the server
to inform if a downlink request has been accepted or rejected by the gateway.
The datagram may optionnaly contain a JSON string to give more details on
acknoledge. If no JSON is present (empty string), this means than no error
occured.

 Bytes  | Function
:------:|---------------------------------------------------------------------
 0      | protocol version = 2
 1-2    | same token as the PULL_RESP packet to acknowledge
 3      | TX_ACK identifier 0x05
 4-11   | Gateway unique identifier (MAC address)
 12-end | [optional] JSON object, starting with {, ending with }, see section 6


## 6. Downstream JSON data structure

The root object of PULL_RESP packet must contain an object named "txpk":

``` json
{
	"txpk": {...}
}
```

That object contain a RF packet to be emitted and associated metadata with the
following fields:

 Name |  Type  | Function
:----:|:------:|--------------------------------------------------------------
 imme | bool   | Send packet immediately (will ignore tmst & tmms)
 tmst | number | Send packet on a certain timestamp value (will ignore tmms)
 tmms | number | Send packet at a certain GPS time (GPS synchronization required)
 freq | number | TX central frequency in MHz (unsigned float, Hz precision)
 rfch | number | Concentrator "RF chain" used for TX (unsigned integer)
 powe | number | TX output power in dBm (unsigned integer, dBm precision)
 modu | string | Modulation identifier "LORA" or "FSK"
 datr | string | LoRa datarate identifier (eg. SF12BW500)
 datr | number | FSK datarate (unsigned, in bits per second)
 codr | string | LoRa ECC coding rate identifier
 fdev | number | FSK frequency deviation (unsigned integer, in Hz)
 ipol | bool   | Lora modulation polarization inversion
 prea | number | RF preamble size (unsigned integer)
 size | number | RF packet payload size in bytes (unsigned integer)
 data | string | Base64 encoded RF packet payload, padding optional
 ncrc | bool   | If true, disable the CRC of the physical layer (optional)
 nhdr | bool   | If true, disable the header of the physical layer (optional)

Most fields are optional.
If a field is omitted, default parameters will be used.

Examples (white-spaces, indentation and newlines added for readability):

``` json
{"txpk":{
	"imme":true,
	"freq":864.123456,
	"rfch":0,
	"powe":14,
	"modu":"LORA",
	"datr":"SF11BW125",
	"codr":"4/6",
	"ipol":false,
	"size":32,
	"data":"H3P3N2i9qc4yt7rK7ldqoeCVJGBybzPY5h1Dd7P7p8v"
}}
```

``` json
{"txpk":{
	"imme":true,
	"freq":861.3,
	"rfch":0,
	"powe":12,
	"modu":"FSK",
	"datr":50000,
	"fdev":3000,
	"size":32,
	"data":"H3P3N2i9qc4yt7rK7ldqoeCVJGBybzPY5h1Dd7P7p8v"
}}
```

In case of error or warning, the root object of TX_ACK packet must contain an
object named "txpk_ack":

``` json
{
	"txpk_ack": {...}
}
```

That object contain status information concerning the associated PULL_RESP packet.

 Name |  Type  | Function
:----:|:------:|-----------------------------------------------------------------------------------------
error | string | Indicates the type of failure that occurred for downlink request (optional)
warn  | string | Indicates that downlink request has been accepted with limitation (optional)
value | string | When a warning is raised, it gives indications about the limitation (optional)
value | number | When a warning is raised, it gives indications about the limitation (optional)

The possible values of the "error" field are:

 Value             | Definition
:-----------------:|---------------------------------------------------------------------
 TOO_LATE          | Rejected because it was already too late to program this packet for downlink
 TOO_EARLY         | Rejected because downlink packet timestamp is too much in advance
 COLLISION_PACKET  | Rejected because there was already a packet programmed in requested timeframe
 COLLISION_BEACON  | Rejected because there was already a beacon planned in requested timeframe
 TX_FREQ           | Rejected because requested frequency is not supported by TX RF chain
 GPS_UNLOCKED      | Rejected because GPS is unlocked, so GPS timestamp cannot be used

The possible values of the "warn" field are:

 Value             | Definition
:-----------------:|---------------------------------------------------------------------
 TX_POWER          | The requested power is not supported by the gateway, the power actually used is given in the value field

Examples (white-spaces, indentation and newlines added for readability):

``` json
{"txpk_ack":{
	"error":"COLLISION_PACKET"
}}
```

``` json
{"txpk_ack":{
	"warn":"TX_POWER",
    "value":20
}}
```

## 7. Revisions

### v1.6 ###
* Added "mid" field in "rxpk" for concentrator modem ID used to demodulate pkt
* Added "foff" field in "rxpk" for frequency offset measured
* Added "rssis" field in "rxpk" for signal RSSI measured.
* Added "ftime" field in "rxpk" for fine timestamping

### v1.5 ###
* Moved TX_POWER from "error" category to "warn" category in "txpk_ack" object

### v1.4 ###
* Added "tmms" field for GPS time as a monotonic number of milliseconds
ellapsed since January 6th, 1980 (GPS Epoch). No leap second.

### v1.3 ###

* Added downlink feedback from gateway to server (PULL_RESP -> TX_ACK)

### v1.2 ###

* Added value of FSK bitrate for upstream.
* Added parameters for FSK bitrate and frequency deviation for downstream.

### v1.1 ###

* Added syntax for status report JSON object on upstream.

### v1.0 ###

* Initial version.
