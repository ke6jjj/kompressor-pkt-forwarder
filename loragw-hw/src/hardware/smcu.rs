pub const REG_CTRL: u16 =       0x0000;
pub const REG_STATUS: u16 =     0x0001;
pub const REG_NON_RF_SIGN_SIZE: u16 =  0x0002;

pub const REG_HARDWARE_VERSION: u16 = 0x0030;
pub const REG_HARDWARE_SERIAL: u16 = 0x0031;
pub const REG_FIRMWARE_VERSION: u16 = 0x0032;
pub const REG_NON_RF_FIFO: u16 = 0x0033;


pub const CONTROL_AFRE: u32 = 0x0000_0001;
