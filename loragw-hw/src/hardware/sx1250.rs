use std::time::Duration;

use crate::lib::*;

#[allow(unused, non_camel_case_types)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum SX1250OpCode {
    CALIBRATE = 0x89,
    CALIBRATE_IMAGE = 0x98,
    CLR_IRQ_STATUS = 0x02,
    STOP_TIMER_ON_PREAMBLE = 0x9F,
    SET_RFSWITCHMODE = 0x9D,
    GET_IRQ_STATUS = 0x12,
    GET_RX_BUFFER_STATUS = 0x13,
    GET_PACKET_STATUS = 0x14,
    READ_BUFFER = 0x1E,
    READ_REGISTER = 0x1D,
    SET_DIO_IRQ_PARAMS = 0x08,
    SET_MODULATION_PARAMS = 0x8B,
    SET_PA_CONFIG = 0x95,
    SET_PACKET_PARAMS = 0x8C,
    SET_PACKET_TYPE = 0x8A,
    SET_RF_FREQUENCY = 0x86,
    SET_BUFFER_BASE_ADDRESS = 0x8F,
    SET_SLEEP = 0x84,
    SET_STANDBY = 0x80,
    SET_RX = 0x82,
    SET_TX = 0x83,
    SET_TX_PARAMS = 0x8E,
    WRITE_BUFFER = 0x0E,
    WRITE_REGISTER = 0x0D,
    SET_TXCONTINUOUSWAVE = 0xD1,
    SET_TXCONTINUOUSPREAMBLE = 0xD2,
    GET_STATUS = 0xC0,
    SET_REGULATORMODE = 0x96,
    SET_FS = 0xC1,
    GET_DEVICE_ERRORS = 0x17,
}

#[allow(unused, non_camel_case_types)]
pub enum StandbyModes {
    STDBY_RC = 0x00,
    STDBY_XOSC = 0x01,
}

pub struct SX1250 {}

impl SX1250 {
    pub fn agc_firmware() -> &'static [u8] {
        use crate::binding::*;
        unsafe {
            let ptr = get_agc_firmware_sx1250();
            let retval = std::ptr::slice_from_raw_parts(ptr, 8192);
            &*retval
        }
    }

    pub async fn calibrate(
        interface: &dyn Interface,
        radio: Radio,
        freq_hz: u32,
    ) -> Result<(), Error> {
        let mut buf = [0_u8; 2];
        interface
            .sx1250_read(radio, SX1250OpCode::GET_STATUS, &mut buf[0..1])
            .await?;

        match freq_hz {
            430_000_000..=440_000_000 => {
                buf[0] = 0x6b;
                buf[1] = 0x6f;
            }
            470_000_000..=510_000_000 => {
                buf[0] = 0x75;
                buf[1] = 0x81;
            }
            779_000_000..=787_000_000 => {
                buf[0] = 0xC1;
                buf[1] = 0xC5;
            }
            863_000_000..=870_000_000 => {
                buf[0] = 0xD7;
                buf[1] = 0xDB;
            }
            902_000_000..=928_000_000 => {
                buf[0] = 0xE1;
                buf[1] = 0xE9;
            }
            _ => {
                return Err(Error::from(GatewayError::Config(format!(
                    "cannot calibrate frequency: {}",
                    freq_hz
                ))))
            }
        }

        interface
            .sx1250_write(radio, SX1250OpCode::CALIBRATE_IMAGE, &buf)
            .await?;

        /* Wait for calibration to complete */
        wait_ms(10).await;

        let mut buf = [0_u8; 3];
        interface
            .sx1250_read(radio, SX1250OpCode::GET_DEVICE_ERRORS, &mut buf)
            .await?;
        if ((buf[2] >> 4) & ((1 << 1) - 1)) != 0 {
            // Calibration error
            return Err(Error::from(GatewayError::Unknown));
        }

        Ok(())
    }

    pub async fn setup(
        interface: &dyn Interface,
        radio: Radio,
        freq_hz: u32,
        single_input_mode: bool,
    ) -> Result<(), Error> {
        {
            /* Set Radio in Standby for calibrations */
            let buf = [StandbyModes::STDBY_RC as u8];
            interface
                .sx1250_write(radio, SX1250OpCode::SET_STANDBY, &buf)
                .await?;
            wait_ms(10).await;
        }

        {
            /* Get status to check Standby mode has been properly set */
            let mut buf = [0_u8];
            interface
                .sx1250_read(radio, SX1250OpCode::GET_STATUS, &mut buf)
                .await?;
            if take_n_bits_from(buf[0], 4, 3) != 0x02 {
                return Err(Error::from(GatewayError::Radio(format!(
                    "failed to set SX1250 in STANDBY_RC mode"
                ))));
            }
        }

        {
            /* Run all calibrations (TCXO) */
            let buf = [0x7f];
            interface
                .sx1250_write(radio, SX1250OpCode::CALIBRATE, &buf)
                .await?;
            wait_ms(10).await;
        }

        {
            /* Set Radio in Standby with XOSC ON */
            let buf = [StandbyModes::STDBY_XOSC as u8];
            interface
                .sx1250_write(radio, SX1250OpCode::SET_STANDBY, &buf)
                .await?;
            wait_ms(10).await;
        }

        {
            /* Get status to check Standby mode has been properly set */
            let mut buf = [0_u8];
            interface
                .sx1250_read(radio, SX1250OpCode::GET_STATUS, &mut buf)
                .await?;
            if take_n_bits_from(buf[0], 4, 3) != 0x03 {
                return Err(Error::from(GatewayError::Radio(format!(
                    "failed to set SX1250 in STANDBY_XOSC mode"
                ))));
            }
        }

        /* Set Bitrate to maximum (to lower TX to FS switch time) */
        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x06_u8, 0xA1_u8, 0x01_u8],
            )
            .await?;

        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x06_u8, 0xA2_u8, 0x00_u8],
            )
            .await?;

        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x06_u8, 0xA3_u8, 0x00_u8],
            )
            .await?;

        /* Configure DIO for Rx */
        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x05_u8, 0x82_u8, 0x00_u8],
            )
            .await?;
        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x05_u8, 0x83_u8, 0x00_u8],
            )
            .await?;
        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x05_u8, 0x84_u8, 0x00_u8],
            )
            .await?;
        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x05_u8, 0x85_u8, 0x00_u8],
            )
            .await?;
        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x05_u8, 0x80_u8, 0x00_u8],
            )
            .await?;

        /* Set fix gain (??) */
        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x08_u8, 0xB6_u8, 0x2A_u8],
            )
            .await?;

        {
            /* Set frequency */
            let freq_reg = freq_to_reg(freq_hz);
            let buf = [
                (freq_reg >> 24) as u8,
                (freq_reg >> 16) as u8,
                (freq_reg >> 8) as u8,
                (freq_reg >> 0) as u8,
            ];
            interface
                .sx1250_write(radio, SX1250OpCode::SET_RF_FREQUENCY, &buf)
                .await?;
        }

        /* Set frequency offset to 0 */
        interface
            .sx1250_write(
                radio,
                SX1250OpCode::WRITE_REGISTER,
                &[0x08, 0x8F, 0x00, 0x00, 0x00],
            )
            .await?;

        /* Set Radio in Rx mode, necessary to give a clock to SX1302 */
        interface
            .sx1250_write(radio, SX1250OpCode::SET_RX, &[0xFF, 0xFF, 0xFF])
            .await?;

        /* Select single input or differential input mode */
        if single_input_mode {
            interface
                .sx1250_write(radio, SX1250OpCode::WRITE_REGISTER, &[0x08, 0xE2, 0x0D])
                .await?;
        }

        /* FPGA_MODE_RX */
        interface
            .sx1250_write(radio, SX1250OpCode::WRITE_REGISTER, &[0x05, 0x87, 0x0B])
            .await?;

        Ok(())
    }
}

async fn wait_ms(ms: u64) {
    tokio::time::sleep(Duration::from_millis(ms)).await;
}

fn take_n_bits_from(b: u8, p: usize, n: usize) -> u8 {
    ((b) >> (p)) & ((1 << (n)) - 1)
}

fn freq_to_reg(freq_hz: u32) -> u32 {
    (freq_hz as u64 * (1 << 25) / 32_000_000) as u32
}
