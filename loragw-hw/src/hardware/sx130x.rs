use crate::lib::*;

use std::{convert::TryInto, mem::MaybeUninit, time::Duration};

use crate::binding::*;
use bytes::Bytes;
use int_enum::IntEnum;

use super::Interface;

mod sx130x_const {
    #![allow(dead_code)]
    pub const GPIO_CFG_REGISTER: u8 = 0x00;
    pub const GPIO_CFG_AGC: u8 = 0x01;
    pub const GPIO_CFG_ARB: u8 = 0x02;
    pub const GPIO_CFG_SPI_EXP_1: u8 = 0x03;
    pub const GPIO_CFG_CSN_SPI_EXP: u8 = 0x04;
    pub const GPIO_CFG_SPI_EXP_2: u8 = 0x05;
    pub const GPIO_CFG_UART: u8 = 0x06;
    pub const GPIO_CFG_SX1255_IQ: u8 = 0x07;
    pub const GPIO_CFG_SX1261_IQ: u8 = 0x08;
    pub const GPIO_CFG_STATUS: u8 = 0x09;
    pub const GPIO_CFG_MBIST: u8 = 0x0A;
    pub const GPIO_CFG_OTP: u8 = 0x0B;

    pub const RX_FINE_TIMING_MODE_LINEAR: u8 = 0x02;

    pub const RX_FREQ_TRACK_OFF: u8 = 0x00;
    pub const RX_FREQ_TRACK_ON: u8 = 0x01;
    pub const RX_FREQ_TRACK_AUTO: u8 = 0x03;

    pub const RX_DFT_PEAK_MODE_AUTO: u8 = 0x03;

    pub const AGC_MEM_ADDR: u16 = 0x0000;
    pub const ARB_MEM_ADDR: u16 = 0x2000;

    pub const SX1302_AGC_RADIO_GAIN_AUTO: u8 = 0xFF;
}

use sx130x_const::*;

pub struct SX130xConfig {
    pub clksrc: u8,
    pub rf_chain_cfg: [RFChainConfig; 2],
    pub if_chain_cfg: [IFChainConfig; 10],
    pub txgain_lut: Vec<TXGain>,
    pub full_duplex: bool,
    pub fine_timestamping: bool,
    pub fine_timestamp_mode: FineTimestampMode,
    pub multisf_datarate: u8,
    pub public: bool,
    pub lbt_enable: bool,
}

impl SX130xConfig {
    pub fn from_board_cfg(cfg: &BoardConfig) -> Result<Self, Error> {
        let mut s = SX130xConfig {
            clksrc: cfg.clksrc,
            rf_chain_cfg: [cfg.radio_0.clone(), cfg.radio_1.clone()],
            if_chain_cfg: Default::default(),
            txgain_lut: Default::default(),
            full_duplex: false,
            fine_timestamping: true,
            fine_timestamp_mode: FineTimestampMode::AllSF,
            multisf_datarate: 0xFF, // enable all SFs
            public: true,
            lbt_enable: false,
        };

        for c in 0..8 {
            if let Some(v) = cfg.extra.get(&format!("chan_multiSF_{}", c)) {
                if let Ok(chain) = serde_json::from_value(v.to_owned()) {
                    s.if_chain_cfg[c] = chain;
                    let freq = s.rf_chain_cfg[chain.rf_chain as usize].freq_hz as i32
                        + chain.relative_freq;
                    info!("Channel {} freq: {:.1}MHz", c, freq as f32 / 1_000_000_f32);
                }
            }
        }

        if let Some(v) = cfg.extra.get("chan_Lora_std") {
            if let Ok(chain) = serde_json::from_value(v.to_owned()) {
                s.if_chain_cfg[8] = chain;
                let freq =
                    s.rf_chain_cfg[chain.rf_chain as usize].freq_hz as i32 + chain.relative_freq;
                info!("Channel 8 freq: {:.1}MHz", freq as f32 / 1_000_000_f32);
            }
        }

        if let Some(lut) = s.rf_chain_cfg[0].tx_gain_lut.as_mut() {
            lut.sort_by_key(|x| x.rf_power);
        }

        if let Some(lut) = s.rf_chain_cfg[1].tx_gain_lut.as_mut() {
            lut.sort_by_key(|x| x.rf_power);
        }

        Ok(s)
    }
}

pub struct SX130x<'a> {
    interface: &'a dyn Interface,
    pub config: &'a SX130xConfig,
}

macro_rules! read_reg {
    ($self:ident, $r:expr) => {{
        let reg = reg_info($r);
        let mut raw = $self.interface.sx130x_read_register(reg.addr).await? as u32;
        raw = raw << (32 - reg.leng - reg.offs);
        if reg.sign {
            raw as i32 >> (32 - reg.leng)
        } else {
            (raw >> (32 - reg.leng)) as i32
        }
    }};
}

macro_rules! write_reg {
    ($self:ident, $r:expr, $v:expr) => {{
        let value = ($v).try_into().unwrap();
        let reg = reg_info($r);
        if reg.leng == 8 && reg.offs == 0 {
            $self
                .interface
                .sx130x_write_register(reg.addr, value)
                .await?
        } else {
            let mask = ((1 << reg.leng) - 1) << reg.offs;
            let value = value << reg.offs;
            $self
                .interface
                .sx130x_rmw_register(reg.addr, value, mask)
                .await?
        }
    }};
}

impl<'a> SX130x<'a> {
    pub fn new(interface: &'a dyn Interface, config: &'a SX130xConfig) -> Self {
        Self { interface, config }
    }

    pub async fn hw_version(&self) -> Result<SX1302Version, Error> {
        let chip = read_reg!(self, SX1302_REG_COMMON_VERSION_VERSION);
        Ok(SX1302Version {
            major: ((chip >> 4) & 0x0F) as u8,
            minor: (chip & 0x0F) as u8,
        })
    }

    pub async fn set_gpio(&self, gpio_reg_val: u8) -> Result<(), Error> {
        write_reg!(self, SX1302_REG_GPIO_GPIO_OUT_L_OUT_VALUE, gpio_reg_val);
        Ok(())
    }

    pub async fn radio_calibrate(&self) -> Result<(), Error> {
        //reset radios
        for (i, _chain) in self
            .config
            .rf_chain_cfg
            .iter()
            .filter(|c| c.enable)
            .enumerate()
        {
            self.radio_reset(i).await?;
            self.radio_set_mode(i).await?;
        }

        // Select the radio which provides the clock to the sx1302
        self.radio_clock_select().await?;

        // Ensure the PA/LNA are disabled
        write_reg!(self, SX1302_REG_AGC_MCU_CTRL_FORCE_HOST_FE_CTRL, 1);
        write_reg!(self, SX1302_REG_AGC_MCU_RF_EN_A_PA_EN, 0);
        write_reg!(self, SX1302_REG_AGC_MCU_RF_EN_A_LNA_EN, 0);

        // Start calibration
        match self.config.rf_chain_cfg[self.config.clksrc as usize].radio_type {
            RadioType::SX1257 | RadioType::SX1255 => {
                todo!("support for SX1255 and SX1257");
            }
            RadioType::SX1250 => {
                debug!("Calibrating SX1250 radios");
                for (i, chain) in self
                    .config
                    .rf_chain_cfg
                    .iter()
                    .filter(|c| c.enable)
                    .enumerate()
                {
                    SX1250::calibrate(
                        self.interface,
                        match i {
                            0 => Radio::RadioA,
                            1 => Radio::RadioB,
                            o => panic!("invalid radio index: {}", o),
                        },
                        chain.freq_hz,
                    )
                    .await?;
                }
            }
            t => {
                return Err(Error::from(GatewayError::Config(format!(
                    "cannot calibrate radio type: {:?}",
                    t
                ))));
            }
        }

        // Release control over Front-End
        write_reg!(self, SX1302_REG_AGC_MCU_CTRL_FORCE_HOST_FE_CTRL, 0);

        Ok(())
    }

    pub async fn radio_reset(&self, radio_idx: usize) -> Result<(), Error> {
        // Switch to SPI clock before resetting the radio
        write_reg!(self, SX1302_REG_COMMON_CTRL0_CLK32_RIF_CTRL, 0x00);

        // Enable the radio
        write_reg!(
            self,
            REG_SELECT(
                radio_idx,
                SX1302_REG_AGC_MCU_RF_EN_A_RADIO_EN,
                SX1302_REG_AGC_MCU_RF_EN_B_RADIO_EN,
            ),
            0x01
        );

        // Select the proper reset sequence depending on the radio type
        let reg_radio_rst = REG_SELECT(
            radio_idx,
            SX1302_REG_AGC_MCU_RF_EN_A_RADIO_RST,
            SX1302_REG_AGC_MCU_RF_EN_B_RADIO_RST,
        );
        write_reg!(self, reg_radio_rst, 0x01);
        wait_ms(500).await;
        write_reg!(self, reg_radio_rst, 0x00);
        wait_ms(10).await;

        match self.config.rf_chain_cfg[radio_idx].radio_type {
            RadioType::SX1255 | RadioType::SX1257 => {
                todo!("support for SX1255 and SX1257");
            }
            RadioType::SX1250 => {
                write_reg!(self, reg_radio_rst, 0x01);
                wait_ms(10).await; // wait for auto calibration to complete
            }
            t => {
                return Err(Error::from(GatewayError::Config(format!(
                    "unsupported radio type: {:?}",
                    t
                ))));
            }
        }

        Ok(())
    }

    pub async fn radio_set_mode(&self, radio_idx: usize) -> Result<(), Error> {
        let reg = REG_SELECT(
            radio_idx,
            SX1302_REG_COMMON_CTRL0_SX1261_MODE_RADIO_A,
            SX1302_REG_COMMON_CTRL0_SX1261_MODE_RADIO_B,
        );
        match self.config.rf_chain_cfg[radio_idx].radio_type {
            RadioType::SX1250 => {
                write_reg!(self, reg, 0x01);
            }
            _ => {
                write_reg!(self, reg, 0x00);
            }
        }
        Ok(())
    }

    pub async fn radio_clock_select(&self) -> Result<(), Error> {
        // Switch SX1302 clock from SPI clock to radio clock of the selected RF chain
        match self.config.clksrc {
            0 => {
                write_reg!(self, SX1302_REG_CLK_CTRL_CLK_SEL_CLK_RADIO_A_SEL, 0x01);
                write_reg!(self, SX1302_REG_CLK_CTRL_CLK_SEL_CLK_RADIO_B_SEL, 0x00);
            }
            1 => {
                write_reg!(self, SX1302_REG_CLK_CTRL_CLK_SEL_CLK_RADIO_A_SEL, 0x00);
                write_reg!(self, SX1302_REG_CLK_CTRL_CLK_SEL_CLK_RADIO_B_SEL, 0x01);
            }
            _ => {
                return Err(Error::from(GatewayError::Unknown));
            }
        }

        // Enable clock dividers
        write_reg!(self, SX1302_REG_CLK_CTRL_CLK_SEL_CLKDIV_EN, 0x01);

        // Set the RIF clock to the 32MHz clock of the radio
        write_reg!(self, SX1302_REG_COMMON_CTRL0_CLK32_RIF_CTRL, 0x01);

        Ok(())
    }

    pub async fn radio_host_ctrl(&self, host_ctrl: bool) -> Result<(), Error> {
        let data = match host_ctrl {
            false => 0x00,
            true => 0x01,
        };
        write_reg!(self, SX1302_REG_COMMON_CTRL0_HOST_RADIO_CTRL, data);
        Ok(())
    }

    pub async fn init(&self) -> Result<(), Error> {
        if self.config.fine_timestamping {
            let model_id = self.get_model().await?;
            info!("SX130x model: {:?}", model_id);
            if model_id != SX130xModel::SX1303 {
                return Err(Error::from(GatewayError::Config(format!(
                    "Fine timestamping is not supported on this ChipModelID: {:?}",
                    model_id
                ))));
            }
        }

        self.timestamp_counter_mode().await?;

        self.config_gpio().await?;

        Ok(())
    }

    pub async fn get_model(&self) -> Result<SX130xModel, Error> {
        write_reg!(self, SX1302_REG_OTP_BYTE_ADDR_ADDR, 0xD0);
        let raw_value = read_reg!(self, SX1302_REG_OTP_RD_DATA_RD_DATA);
        let raw_value: u8 = raw_value.try_into().map_err(|_e| GatewayError::Unknown)?;

        Ok(SX130xModel::from_int(raw_value).map_err(|_e| GatewayError::Unknown)?)
    }

    async fn timestamp_counter_mode(&self) -> Result<(), Error> {
        if self.config.fine_timestamping == false {
            info!("using legacy timestamp");
            /* Latch end-of-packet timestamp (sx1301 compatibility) */
            write_reg!(self, SX1302_REG_RX_TOP_RX_BUFFER_LEGACY_TIMESTAMP, 0x01);
        } else {
            const PRECISION_TIMESTAMP_TS_METRICS_MAX: u8 = 32;
            const PRECISION_TIMESTAMP_NB_SYMBOLS: u8 = 0;

            info!("using precision timestamp");
            /* Latch end-of-preamble timestamp */
            write_reg!(self, SX1302_REG_RX_TOP_RX_BUFFER_LEGACY_TIMESTAMP, 0x00);
            write_reg!(
                self,
                SX1302_REG_RX_TOP_RX_BUFFER_TIMESTAMP_CFG_MAX_TS_METRICS,
                PRECISION_TIMESTAMP_TS_METRICS_MAX
            );

            write_reg!(self, SX1302_REG_RX_TOP_TIMESTAMP_ENABLE, 0x01);
            write_reg!(
                self,
                SX1302_REG_RX_TOP_TIMESTAMP_NB_SYMB,
                PRECISION_TIMESTAMP_NB_SYMBOLS
            );
        }

        Ok(())
    }

    async fn config_gpio(&self) -> Result<(), Error> {
        write_reg!(
            self,
            SX1302_REG_GPIO_GPIO_SEL_0_SELECTION,
            GPIO_CFG_REGISTER
        ); /* GPIO_0 => CONFIG_DONE */
        write_reg!(
            self,
            SX1302_REG_GPIO_GPIO_SEL_1_SELECTION,
            GPIO_CFG_REGISTER
        ); /* GPIO_1 => UNUSED */
        write_reg!(self, SX1302_REG_GPIO_GPIO_SEL_2_SELECTION, GPIO_CFG_STATUS); /* GPIO_2 => Tx ON */
        write_reg!(
            self,
            SX1302_REG_GPIO_GPIO_SEL_3_SELECTION,
            GPIO_CFG_REGISTER
        ); /* GPIO_3 => UNUSED */
        write_reg!(self, SX1302_REG_GPIO_GPIO_SEL_4_SELECTION, GPIO_CFG_STATUS); /* GPIO_4 => RX ON (PKT_RECEIVE_TOGGLE_OUT) */
        write_reg!(
            self,
            SX1302_REG_GPIO_GPIO_SEL_5_SELECTION,
            GPIO_CFG_REGISTER
        ); /* GPIO_5 => UNUSED */
        write_reg!(
            self,
            SX1302_REG_GPIO_GPIO_SEL_6_SELECTION,
            GPIO_CFG_REGISTER
        ); /* GPIO_6 => UNUSED */
        write_reg!(self, SX1302_REG_GPIO_GPIO_SEL_7_SELECTION, GPIO_CFG_AGC); /* GPIO_7 => USED FOR LBT (MUST BE INPUT) */
        write_reg!(self, SX1302_REG_GPIO_GPIO_DIR_L_DIRECTION, 0x7F); /* GPIO output direction (0 for input and 1 for output) */
        Ok(())
    }

    pub async fn pa_lna_lut_config(&self) -> Result<(), Error> {
        /* Configure LUT Table A */
        if self.config.full_duplex {
            write_reg!(self, SX1302_REG_AGC_MCU_LUT_TABLE_A_PA_LUT, 0x0C);
            write_reg!(self, SX1302_REG_AGC_MCU_LUT_TABLE_A_LNA_LUT, 0x0F);
        } else {
            write_reg!(self, SX1302_REG_AGC_MCU_LUT_TABLE_A_PA_LUT, 0x04);
            write_reg!(self, SX1302_REG_AGC_MCU_LUT_TABLE_A_LNA_LUT, 0x02);
        }

        /* Configure LUT Table B */
        write_reg!(self, SX1302_REG_AGC_MCU_LUT_TABLE_B_PA_LUT, 0x04);
        write_reg!(self, SX1302_REG_AGC_MCU_LUT_TABLE_B_LNA_LUT, 0x02);

        Ok(())
    }

    pub async fn radio_fe_configure(&self) -> Result<(), Error> {
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_RSSI_BB_FILTER_ALPHA_RADIO_A_RSSI_BB_FILTER_ALPHA,
            0x03
        );
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_RSSI_DEC_FILTER_ALPHA_RADIO_A_RSSI_DEC_FILTER_ALPHA,
            0x07
        );
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_RSSI_BB_FILTER_ALPHA_RADIO_B_RSSI_BB_FILTER_ALPHA,
            0x03
        );
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_RSSI_DEC_FILTER_ALPHA_RADIO_B_RSSI_DEC_FILTER_ALPHA,
            0x07
        );

        write_reg!(
            self,
            SX1302_REG_RADIO_FE_RSSI_DB_DEF_RADIO_A_RSSI_DB_DEFAULT_VALUE,
            23
        );
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_RSSI_DEC_DEF_RADIO_A_RSSI_DEC_DEFAULT_VALUE,
            66
        );
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_RSSI_DB_DEF_RADIO_B_RSSI_DB_DEFAULT_VALUE,
            23
        );
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_RSSI_DEC_DEF_RADIO_B_RSSI_DEC_DEFAULT_VALUE,
            66
        );

        write_reg!(self, SX1302_REG_RADIO_FE_CTRL0_RADIO_A_DC_NOTCH_EN, 1);
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_CTRL0_RADIO_A_HOST_FILTER_GAIN,
            0x0b
        );
        write_reg!(self, SX1302_REG_RADIO_FE_CTRL0_RADIO_B_DC_NOTCH_EN, 1);
        write_reg!(
            self,
            SX1302_REG_RADIO_FE_CTRL0_RADIO_B_HOST_FILTER_GAIN,
            0x0b
        );

        Ok(())
    }

    pub async fn channelizer_configure(&self, fix_gain: bool) -> Result<(), Error> {
        let mut channels_mask: u8 = 0;

        /* Select which radio is connected to each multi-SF channel */
        for i in 0..8 {
            channels_mask |= self.config.if_chain_cfg[i].rf_chain << i;
        }
        write_reg!(
            self,
            SX1302_REG_RX_TOP_RADIO_SELECT_RADIO_SELECT,
            channels_mask
        );

        /* Select which radio is connected to the LoRa service channel */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_LORA_SERVICE_RADIO_SEL_RADIO_SELECT,
            self.config.if_chain_cfg[8].rf_chain
        );

        /* Select which radio is connected to the FSK channel */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FSK_CFG_3_RADIO_SELECT,
            self.config.if_chain_cfg[9].rf_chain
        );

        /* Configure multi-SF channels IF frequencies */
        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[0].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_0_MSB_IF_FREQ_0,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_0_LSB_IF_FREQ_0,
            (if_freq >> 0) & 0x000000FF
        );

        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[1].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_1_MSB_IF_FREQ_1,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_1_LSB_IF_FREQ_1,
            (if_freq >> 0) & 0x000000FF
        );

        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[2].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_2_MSB_IF_FREQ_2,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_2_LSB_IF_FREQ_2,
            (if_freq >> 0) & 0x000000FF
        );

        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[3].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_3_MSB_IF_FREQ_3,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_3_LSB_IF_FREQ_3,
            (if_freq >> 0) & 0x000000FF
        );

        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[4].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_4_MSB_IF_FREQ_4,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_4_LSB_IF_FREQ_4,
            (if_freq >> 0) & 0x000000FF
        );

        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[5].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_5_MSB_IF_FREQ_5,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_5_LSB_IF_FREQ_5,
            (if_freq >> 0) & 0x000000FF
        );

        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[6].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_6_MSB_IF_FREQ_6,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_6_LSB_IF_FREQ_6,
            (if_freq >> 0) & 0x000000FF
        );

        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[7].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_7_MSB_IF_FREQ_7,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_7_LSB_IF_FREQ_7,
            (if_freq >> 0) & 0x000000FF
        );

        /* Configure LoRa service channel IF frequency */
        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[8].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_LORA_SERVICE_FREQ_MSB_IF_FREQ_0,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_LORA_SERVICE_FREQ_LSB_IF_FREQ_0,
            (if_freq >> 0) & 0x000000FF
        );

        /* Configure FSK channel IF frequency */
        let if_freq = IF_HZ_TO_REG(self.config.if_chain_cfg[9].relative_freq);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FSK_FREQ_MSB_IF_FREQ_0,
            (if_freq >> 8) & 0x0000001F
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FSK_FREQ_LSB_IF_FREQ_0,
            (if_freq >> 0) & 0x000000FF
        );

        /* Set the low pass filtering corner frequency for RSSI indicator */
        write_reg!(self, SX1302_REG_RX_TOP_RSSI_CONTROL_RSSI_FILTER_ALPHA, 0x05);

        /* Set the channelizer RSSI reset value */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_RSSI_DEF_VALUE_CHAN_RSSI_DEF_VALUE,
            85
        );

        /* Force channelizer in fix gain, or let it be controlled by AGC */
        if fix_gain == true {
            write_reg!(self, SX1302_REG_RX_TOP_CHANN_DAGC_CFG5_CHAN_DAGC_MODE, 0x00);
            write_reg!(self, SX1302_REG_RX_TOP_GAIN_CONTROL_CHAN_GAIN, 5);
        } else {
            /* Allow the AGC to control gains */
            write_reg!(self, SX1302_REG_RX_TOP_CHANN_DAGC_CFG5_CHAN_DAGC_MODE, 0x01);
            /* Disable the internal DAGC */
            write_reg!(
                self,
                SX1302_REG_RX_TOP_CHANN_DAGC_CFG1_CHAN_DAGC_THRESHOLD_HIGH,
                255
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_CHANN_DAGC_CFG2_CHAN_DAGC_THRESHOLD_LOW,
                0
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_CHANN_DAGC_CFG3_CHAN_DAGC_MAX_ATTEN,
                15
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_CHANN_DAGC_CFG3_CHAN_DAGC_MIN_ATTEN,
                0
            );
        }

        Ok(())
    }

    pub async fn lora_correlator_configure(&self) -> Result<(), Error> {
        write_reg!(self, SX1302_REG_RX_TOP_SF5_CFG2_ACC_PNR, 52);
        write_reg!(self, SX1302_REG_RX_TOP_SF5_CFG4_MSP_PNR, 24);
        write_reg!(self, SX1302_REG_RX_TOP_SF5_CFG6_MSP_PEAK_NB, 7);
        write_reg!(self, SX1302_REG_RX_TOP_SF5_CFG7_MSP2_PEAK_NB, 5);

        write_reg!(self, SX1302_REG_RX_TOP_SF6_CFG2_ACC_PNR, 52);
        write_reg!(self, SX1302_REG_RX_TOP_SF6_CFG4_MSP_PNR, 24);
        write_reg!(self, SX1302_REG_RX_TOP_SF6_CFG6_MSP_PEAK_NB, 7);
        write_reg!(self, SX1302_REG_RX_TOP_SF6_CFG7_MSP2_PEAK_NB, 5);

        write_reg!(self, SX1302_REG_RX_TOP_SF7_CFG2_ACC_PNR, 52);
        write_reg!(self, SX1302_REG_RX_TOP_SF7_CFG4_MSP_PNR, 24);
        write_reg!(self, SX1302_REG_RX_TOP_SF7_CFG6_MSP_PEAK_NB, 7);
        write_reg!(self, SX1302_REG_RX_TOP_SF7_CFG7_MSP2_PEAK_NB, 5);

        write_reg!(self, SX1302_REG_RX_TOP_SF8_CFG2_ACC_PNR, 52);
        write_reg!(self, SX1302_REG_RX_TOP_SF8_CFG4_MSP_PNR, 24);
        write_reg!(self, SX1302_REG_RX_TOP_SF8_CFG6_MSP_PEAK_NB, 7);
        write_reg!(self, SX1302_REG_RX_TOP_SF8_CFG7_MSP2_PEAK_NB, 5);

        write_reg!(self, SX1302_REG_RX_TOP_SF9_CFG2_ACC_PNR, 52);
        write_reg!(self, SX1302_REG_RX_TOP_SF9_CFG4_MSP_PNR, 24);
        write_reg!(self, SX1302_REG_RX_TOP_SF9_CFG6_MSP_PEAK_NB, 7);
        write_reg!(self, SX1302_REG_RX_TOP_SF9_CFG7_MSP2_PEAK_NB, 5);

        write_reg!(self, SX1302_REG_RX_TOP_SF10_CFG2_ACC_PNR, 52);
        write_reg!(self, SX1302_REG_RX_TOP_SF10_CFG4_MSP_PNR, 24);
        write_reg!(self, SX1302_REG_RX_TOP_SF10_CFG6_MSP_PEAK_NB, 7);
        write_reg!(self, SX1302_REG_RX_TOP_SF10_CFG7_MSP2_PEAK_NB, 5);

        write_reg!(self, SX1302_REG_RX_TOP_SF11_CFG2_ACC_PNR, 52);
        write_reg!(self, SX1302_REG_RX_TOP_SF11_CFG4_MSP_PNR, 24);
        write_reg!(self, SX1302_REG_RX_TOP_SF11_CFG6_MSP_PEAK_NB, 7);
        write_reg!(self, SX1302_REG_RX_TOP_SF11_CFG7_MSP2_PEAK_NB, 5);

        write_reg!(self, SX1302_REG_RX_TOP_SF12_CFG2_ACC_PNR, 52);
        write_reg!(self, SX1302_REG_RX_TOP_SF12_CFG4_MSP_PNR, 24);
        write_reg!(self, SX1302_REG_RX_TOP_SF12_CFG6_MSP_PEAK_NB, 7);
        write_reg!(self, SX1302_REG_RX_TOP_SF12_CFG7_MSP2_PEAK_NB, 5);

        write_reg!(
            self,
            SX1302_REG_RX_TOP_CORRELATOR_ENABLE_ONLY_FIRST_DET_EDGE_ENABLE_ONLY_FIRST_DET_EDGE,
            0xFF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_CORRELATOR_ENABLE_ACC_CLEAR_ENABLE_CORR_ACC_CLEAR,
            0xFF
        );

        /* Enabled selected spreading factors */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_CORRELATOR_SF_EN_CORR_SF_EN,
            self.config.multisf_datarate
        );

        /* Enable correlator if channel is enabled (1 correlator per channel) */
        let mut channels_mask: u8 = 0x00;
        for i in 0..8 {
            channels_mask |= if self.config.if_chain_cfg[i].enable {
                1
            } else {
                0
            } << i;
        }

        write_reg!(
            self,
            SX1302_REG_RX_TOP_CORR_CLOCK_ENABLE_CLK_EN,
            channels_mask
        );
        write_reg!(self, SX1302_REG_RX_TOP_CORRELATOR_EN_CORR_EN, channels_mask);

        Ok(())
    }

    pub async fn lora_modem_configure(&self) -> Result<(), Error> {
        let radio_freq_hz = self.config.rf_chain_cfg[0].freq_hz;

        write_reg!(self, SX1302_REG_RX_TOP_DC_NOTCH_CFG1_ENABLE, 0x00);
        write_reg!(self, SX1302_REG_RX_TOP_RX_DFE_AGC1_FORCE_DEFAULT_FIR, 0x01);
        write_reg!(self, SX1302_REG_RX_TOP_DAGC_CFG_GAIN_DROP_COMP, 0x01);
        write_reg!(self, SX1302_REG_RX_TOP_DAGC_CFG_TARGET_LVL, 0x01);

        /* Enable full modems */
        write_reg!(self, SX1302_REG_OTP_MODEM_EN_0_MODEM_EN, 0xFF);

        /* Enable limited modems */
        write_reg!(self, SX1302_REG_OTP_MODEM_EN_1_MODEM_EN, 0xFF);

        /* Configure coarse sync between correlators and modems */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_SYNC_DELTA_MSB_MODEM_SYNC_DELTA,
            0
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_SYNC_DELTA_LSB_MODEM_SYNC_DELTA,
            126
        );

        /* Configure fine sync offset for each channel */
        write_reg!(
            self,
            SX1302_REG_ARB_MCU_CHANNEL_SYNC_OFFSET_01_CHANNEL_0_OFFSET,
            1
        );
        write_reg!(
            self,
            SX1302_REG_ARB_MCU_CHANNEL_SYNC_OFFSET_01_CHANNEL_1_OFFSET,
            5
        );
        write_reg!(
            self,
            SX1302_REG_ARB_MCU_CHANNEL_SYNC_OFFSET_23_CHANNEL_2_OFFSET,
            9
        );
        write_reg!(
            self,
            SX1302_REG_ARB_MCU_CHANNEL_SYNC_OFFSET_23_CHANNEL_3_OFFSET,
            13
        );
        write_reg!(
            self,
            SX1302_REG_ARB_MCU_CHANNEL_SYNC_OFFSET_45_CHANNEL_4_OFFSET,
            1
        );
        write_reg!(
            self,
            SX1302_REG_ARB_MCU_CHANNEL_SYNC_OFFSET_45_CHANNEL_5_OFFSET,
            5
        );
        write_reg!(
            self,
            SX1302_REG_ARB_MCU_CHANNEL_SYNC_OFFSET_67_CHANNEL_6_OFFSET,
            9
        );
        write_reg!(
            self,
            SX1302_REG_ARB_MCU_CHANNEL_SYNC_OFFSET_67_CHANNEL_7_OFFSET,
            13
        );

        /* Configure PPM offset */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_PPM_OFFSET1_PPM_OFFSET_SF5,
            0x00
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_PPM_OFFSET1_PPM_OFFSET_SF6,
            0x00
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_PPM_OFFSET1_PPM_OFFSET_SF7,
            0x00
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_PPM_OFFSET1_PPM_OFFSET_SF8,
            0x00
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_PPM_OFFSET2_PPM_OFFSET_SF9,
            0x00
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_PPM_OFFSET2_PPM_OFFSET_SF10,
            0x00
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_PPM_OFFSET2_PPM_OFFSET_SF11,
            0x01
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_MODEM_PPM_OFFSET2_PPM_OFFSET_SF12,
            0x01
        );

        /* Improve SF5 and SF6 performances */
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_A_1_GAIN_P_AUTO, 3); // Default is 1
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_A_1_GAIN_P_PAYLOAD, 3); // Default is 2

        /* Improve SF11/SF12 performances */
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_A_5_GAIN_I_EN_SF11, 1);
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_A_5_GAIN_I_EN_SF12, 1);

        /* Set threshold for 1bin correction (CAN-314) */
        write_reg!(self, SX1302_REG_RX_TOP_FREQ_TRACK4_FREQ_SYNCH_THR, 15);

        /* Configure modems for best tracking (best demodulation) */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_A_0_FREQ_TRACK_EN_SF5,
            RX_FREQ_TRACK_AUTO
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_A_0_FREQ_TRACK_EN_SF6,
            RX_FREQ_TRACK_AUTO
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_A_0_FREQ_TRACK_EN_SF7,
            RX_FREQ_TRACK_AUTO
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_A_0_FREQ_TRACK_EN_SF8,
            RX_FREQ_TRACK_AUTO
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_A_1_FREQ_TRACK_EN_SF9,
            RX_FREQ_TRACK_AUTO
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_A_1_FREQ_TRACK_EN_SF10,
            RX_FREQ_TRACK_AUTO
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_A_1_FREQ_TRACK_EN_SF11,
            RX_FREQ_TRACK_AUTO
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_A_1_FREQ_TRACK_EN_SF12,
            RX_FREQ_TRACK_AUTO
        );

        /* Configure modems for best timestamping (only valid when double demodulation is enabled) */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_B_0_FREQ_TRACK_EN_SF5,
            RX_FREQ_TRACK_OFF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_B_0_FREQ_TRACK_EN_SF6,
            RX_FREQ_TRACK_OFF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_B_0_FREQ_TRACK_EN_SF7,
            RX_FREQ_TRACK_OFF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_B_0_FREQ_TRACK_EN_SF8,
            RX_FREQ_TRACK_OFF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_B_1_FREQ_TRACK_EN_SF9,
            RX_FREQ_TRACK_OFF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_B_1_FREQ_TRACK_EN_SF10,
            RX_FREQ_TRACK_OFF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_B_1_FREQ_TRACK_EN_SF11,
            RX_FREQ_TRACK_OFF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TRACK_B_1_FREQ_TRACK_EN_SF12,
            RX_FREQ_TRACK_OFF
        );
        /* -- */
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_5_GAIN_I_EN_SF11, 0);
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_5_GAIN_I_EN_SF12, 0);
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_0_ROUNDING, 1);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FINE_TIMING_B_0_MODE,
            RX_FINE_TIMING_MODE_LINEAR
        );
        /* -- */
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_1_GAIN_P_AUTO, 0);
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_1_GAIN_P_PREAMB, 6);
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_1_GAIN_P_PAYLOAD, 2);
        /* -- */
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_2_GAIN_I_AUTO, 0);
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_2_GAIN_I_PREAMB, 1);
        write_reg!(self, SX1302_REG_RX_TOP_FINE_TIMING_B_2_GAIN_I_PAYLOAD, 0);

        /* Set preamble size to 10 (to handle 12 for SF5/SF6 and 8 for SF7->SF12) */
        write_reg!(self, SX1302_REG_RX_TOP_TXRX_CFG7_PREAMBLE_SYMB_NB, 0); /* MSB */
        write_reg!(self, SX1302_REG_RX_TOP_TXRX_CFG6_PREAMBLE_SYMB_NB, 10); /* LSB */

        /* Freq2TimeDrift computation */
        let mut mantissa = 0_u16;
        let mut exponent = 0_u8;
        calculate_freq_to_time_drift(
            radio_freq_hz,
            Bandwidth::BW125,
            &mut mantissa,
            &mut exponent,
        );
        debug!(
            "Freq2TimeDrift MultiSF: Mantissa = {} ({:02X}, {:02X}), Exponent = {} ({:02X})",
            mantissa,
            (mantissa >> 8) & 0x00FF,
            (mantissa) & 0x00FF,
            exponent,
            exponent
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TO_TIME0_FREQ_TO_TIME_DRIFT_MANT,
            (mantissa >> 8) & 0x00FF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TO_TIME1_FREQ_TO_TIME_DRIFT_MANT,
            (mantissa) & 0x00FF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TO_TIME2_FREQ_TO_TIME_DRIFT_EXP,
            exponent
        );

        /* Time drift compensation */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_FREQ_TO_TIME3_FREQ_TO_TIME_INVERT_TIME_SYMB,
            1
        );

        /* DFT peak mode : set to AUTO, check timestamp_counter_correction() if changed */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_RX_CFG0_DFT_PEAK_EN,
            RX_DFT_PEAK_MODE_AUTO
        );

        Ok(())
    }

    pub async fn lora_service_correlator_configure(&self) -> Result<(), Error> {
        /* Common config for all SF */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_MSP2_MSP_PEAK_NB,
            7
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_MSP2_MSP2_PEAK_NB,
            5
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_USE_GAIN_SYMB,
            1
        );

        match self.config.if_chain_cfg[8].spread_factor {
            Some(SpreadingFactor::SF5) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_FINE_SYNCH_EN,
                    1
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_ACC_PNR,
                    52
                );
            }
            Some(SpreadingFactor::SF6) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_FINE_SYNCH_EN,
                    1
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_ACC_PNR,
                    52
                );
            }
            Some(SpreadingFactor::SF7) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_FINE_SYNCH_EN,
                    0
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_ACC_PNR,
                    52
                );
            }
            Some(SpreadingFactor::SF8) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_FINE_SYNCH_EN,
                    0
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_ACC_PNR,
                    52
                );
            }
            Some(SpreadingFactor::SF9) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_FINE_SYNCH_EN,
                    0
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_ACC_PNR,
                    52
                );
            }
            Some(SpreadingFactor::SF10) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_FINE_SYNCH_EN,
                    0
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_ACC_PNR,
                    52
                );
            }
            Some(SpreadingFactor::SF11) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_FINE_SYNCH_EN,
                    0
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_ACC_PNR,
                    52
                );
            }
            Some(SpreadingFactor::SF12) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_FINE_SYNCH_EN,
                    0
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DETECT_ACC1_ACC_PNR,
                    52
                );
            }
            None => {
                return Err(Error::from(GatewayError::Config(
                    "Must have a spreading factor".to_owned(),
                )));
            }
        }
        Ok(())
    }

    pub async fn lora_service_modem_configure(&self) -> Result<(), Error> {
        fn get_reg_value(bw: &Bandwidth) -> u8 {
            match bw {
                Bandwidth::BW125 => 0x04,
                Bandwidth::BW250 => 0x05,
                Bandwidth::BW500 => 0x06,
            }
        }

        let bandwidth = Bandwidth::from_hz(self.config.if_chain_cfg[8].bandwidth.ok_or(
            GatewayError::Config("bandwidth must be specified".to_owned()),
        )?)
        .map_err(|e| GatewayError::Config(format!("{:?}", e)))?;

        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DC_NOTCH_CFG1_ENABLE,
            0x00
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_RX_DFE_AGC1_FORCE_DEFAULT_FIR,
            0x01
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DAGC_CFG_GAIN_DROP_COMP,
            0x01
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_DAGC_CFG_TARGET_LVL,
            0x01
        );

        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING1_GAIN_P_AUTO,
            0x03
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING2_GAIN_I_PAYLOAD,
            0x03
        );

        match self.config.if_chain_cfg[8].spread_factor {
            Some(SpreadingFactor::SF5) | Some(SpreadingFactor::SF6) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING1_GAIN_P_PREAMB,
                    0x04
                ); // Default value
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING2_GAIN_I_EN,
                    0x00
                ); // Default value
            }
            Some(SpreadingFactor::SF7)
            | Some(SpreadingFactor::SF8)
            | Some(SpreadingFactor::SF9)
            | Some(SpreadingFactor::SF10) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING1_GAIN_P_PREAMB,
                    0x06
                );
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING2_GAIN_I_EN,
                    0x00
                );
            }
            Some(SpreadingFactor::SF11) | Some(SpreadingFactor::SF12) => {
                write_reg!(
                    self,
                    SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING1_GAIN_P_PREAMB,
                    0x07
                );
                match bandwidth {
                    Bandwidth::BW125 => {
                        write_reg!(
                            self,
                            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING2_GAIN_I_EN,
                            0x01
                        );
                    }
                    Bandwidth::BW250 => {
                        write_reg!(
                            self,
                            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING2_GAIN_I_EN,
                            0x02
                        );
                    }
                    Bandwidth::BW500 => {
                        write_reg!(
                            self,
                            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FINE_TIMING2_GAIN_I_EN,
                            0x03
                        );
                    }
                }
            }
            None => {
                return Err(Error::from(GatewayError::Config(
                    "Must have a spreading factor".to_owned(),
                )))
            }
        }

        let implicit_hdr = self.config.if_chain_cfg[8]
            .implicit_hdr
            .ok_or(GatewayError::Config(
                "implicit_hdr must be specified".to_owned(),
            ))?;
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_IMPLICIT_HEADER,
            match implicit_hdr {
                true => 1,
                false => 0,
            }
        );
        let implicit_crc_en =
            self.config.if_chain_cfg[8]
                .implicit_crc_en
                .ok_or(GatewayError::Config(
                    "implicit_crc_en must be specified".to_owned(),
                ))?;
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_CRC_EN,
            match implicit_crc_en {
                true => 1,
                false => 0,
            }
        );
        let implicit_coderate =
            self.config.if_chain_cfg[8]
                .implicit_coderate
                .ok_or(GatewayError::Config(
                    "implicit_coderate must be specified".to_owned(),
                ))?;
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG1_CODING_RATE,
            implicit_coderate
        );
        let implicit_payload_length =
            self.config.if_chain_cfg[8]
                .implicit_payload_length
                .ok_or(GatewayError::Config(
                    "implicit_payload_length must be specified".to_owned(),
                ))?;
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG3_PAYLOAD_LENGTH,
            implicit_payload_length
        );

        let sf = self.config.if_chain_cfg[8]
            .spread_factor
            .ok_or(GatewayError::Config(
                "spreading factor must be specified".to_owned(),
            ))?;
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG0_MODEM_SF,
            sf.factor()
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG0_MODEM_BW,
            get_reg_value(&bandwidth)
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG1_PPM_OFFSET,
            match SET_PPM_ON(bandwidth, sf) {
                true => 1,
                false => 0,
            }
        );

        /* Set preamble size to 8 for SF7->SF12 and to 12 for SF5->SF6 (aligned with end-device drivers) */
        let preamble_nb_symb = if (sf == SpreadingFactor::SF5) || (sf == SpreadingFactor::SF6) {
            12
        } else {
            8
        };
        info!(
            "LoRa Service modem: configuring preamble size to {} symbols",
            preamble_nb_symb
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG7_PREAMBLE_SYMB_NB,
            (preamble_nb_symb >> 8) & 0xFF
        ); /* MSB */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG6_PREAMBLE_SYMB_NB,
            (preamble_nb_symb >> 0) & 0xFF
        ); /* LSB */

        /* Freq2TimeDrift computation */
        let radio_freq_hz = self.config.rf_chain_cfg[0].freq_hz;
        let mut mantissa = 0_u16;
        let mut exponent = 0_u8;
        calculate_freq_to_time_drift(radio_freq_hz, bandwidth, &mut mantissa, &mut exponent);
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FREQ_TO_TIME0_FREQ_TO_TIME_DRIFT_MANT,
            (mantissa >> 8) & 0x00FF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FREQ_TO_TIME1_FREQ_TO_TIME_DRIFT_MANT,
            (mantissa) & 0x00FF
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FREQ_TO_TIME2_FREQ_TO_TIME_DRIFT_EXP,
            exponent
        );
        debug!(
            "Freq2TimeDrift SingleSF: Mantissa = {} ({:02X}, {:02X}), Exponent = {} ({:02X})",
            mantissa,
            (mantissa >> 8) & 0x00FF,
            (mantissa) & 0x00FF,
            exponent,
            exponent
        );

        /* Time drift compensation */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FREQ_TO_TIME3_FREQ_TO_TIME_INVERT_TIME_SYMB,
            1
        );

        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_RX_DFE_AGC2_DAGC_IN_COMP,
            1
        );

        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG1_MODEM_EN,
            1
        );
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_CADRXTX,
            1
        );

        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_TXRX_CFG2_MODEM_START,
            1
        );

        /* DFT peak mode : set to AUTO, check timestamp_counter_correction() if changed */
        write_reg!(
            self,
            SX1302_REG_RX_TOP_LORA_SERVICE_FSK_RX_CFG0_DFT_PEAK_EN,
            RX_DFT_PEAK_MODE_AUTO
        );

        Ok(())
    }

    pub async fn lora_syncword(&self) -> Result<(), Error> {
        let lora_service_sf =
            self.config.if_chain_cfg[8]
                .spread_factor
                .ok_or(GatewayError::Config(
                    "must have spreading factor".to_owned(),
                ))?;

        /* Multi-SF modem configuration */
        debug!("configuring LoRa (Multi-SF) SF5->SF6 with syncword PRIVATE (0x12)");
        write_reg!(self, SX1302_REG_RX_TOP_FRAME_SYNCH0_SF5_PEAK1_POS_SF5, 2);
        write_reg!(self, SX1302_REG_RX_TOP_FRAME_SYNCH1_SF5_PEAK2_POS_SF5, 4);
        write_reg!(self, SX1302_REG_RX_TOP_FRAME_SYNCH0_SF6_PEAK1_POS_SF6, 2);
        write_reg!(self, SX1302_REG_RX_TOP_FRAME_SYNCH1_SF6_PEAK2_POS_SF6, 4);
        if self.config.public {
            debug!("configuring LoRa (Multi-SF) SF7->SF12 with syncword PUBLIC (0x34)");
            write_reg!(
                self,
                SX1302_REG_RX_TOP_FRAME_SYNCH0_SF7TO12_PEAK1_POS_SF7TO12,
                6
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_FRAME_SYNCH1_SF7TO12_PEAK2_POS_SF7TO12,
                8
            );
        } else {
            debug!("configuring LoRa (Multi-SF) SF7->SF12 with syncword PRIVATE (0x12)");
            write_reg!(
                self,
                SX1302_REG_RX_TOP_FRAME_SYNCH0_SF7TO12_PEAK1_POS_SF7TO12,
                2
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_FRAME_SYNCH1_SF7TO12_PEAK2_POS_SF7TO12,
                4
            );
        }

        /* LoRa Service modem configuration */
        if (self.config.public == false)
            || (lora_service_sf == SpreadingFactor::SF5)
            || (lora_service_sf == SpreadingFactor::SF6)
        {
            debug!(
                "configuring LoRa (Service) {:?} with syncword PRIVATE (0x12)",
                lora_service_sf
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FRAME_SYNCH0_PEAK1_POS,
                2
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FRAME_SYNCH1_PEAK2_POS,
                4
            );
        } else {
            debug!(
                "configuring LoRa (Service) {:?} with syncword PUBLIC (0x34)",
                lora_service_sf
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FRAME_SYNCH0_PEAK1_POS,
                6
            );
            write_reg!(
                self,
                SX1302_REG_RX_TOP_LORA_SERVICE_FSK_FRAME_SYNCH1_PEAK2_POS,
                8
            );
        }
        Ok(())
    }

    pub async fn modem_enable(&self) -> Result<(), Error> {
        /* Enable LoRa multi-SF modems */
        write_reg!(self, SX1302_REG_COMMON_GEN_CONCENTRATOR_MODEM_ENABLE, 0x01);

        /* Enable LoRa service modem */
        write_reg!(self, SX1302_REG_COMMON_GEN_MBWSSF_MODEM_ENABLE, 0x01);

        /* Enable FSK modem */
        write_reg!(self, SX1302_REG_COMMON_GEN_FSK_MODEM_ENABLE, 0x01);

        /* Enable RX */
        write_reg!(self, SX1302_REG_COMMON_GEN_GLOBAL_EN, 0x01);
        Ok(())
    }

    pub async fn agc_load_firmware(&self, fw: &[u8]) -> Result<(), Error> {
        use memcmp::Memcmp;

        assert!(fw.len() == 8192, "agc firmware must be 8192 bytes");

        /* Take control over AGC MCU */
        write_reg!(self, SX1302_REG_AGC_MCU_CTRL_MCU_CLEAR, 0x01);
        write_reg!(self, SX1302_REG_AGC_MCU_CTRL_HOST_PROG, 0x01);
        write_reg!(self, SX1302_REG_COMMON_PAGE_PAGE, 0x00);

        /* Write AGC fw in AGC MEM */
        self.interface
            .sx130x_mem_write(AGC_MEM_ADDR, bytes::Bytes::copy_from_slice(fw))
            .await?;

        /* Read back and check */
        debug!("read back AGC...");
        let fw_check = self
            .interface
            .sx130x_mem_read(AGC_MEM_ADDR, 8192, false)
            .await?;

        if !fw.memcmp(&fw_check[..]) {
            return Err(Error::from(GatewayError::Radio(format!(
                "AGC fw read/write check failed"
            ))));
        }

        /* Release control over AGC MCU */
        write_reg!(self, SX1302_REG_AGC_MCU_CTRL_HOST_PROG, 0x00);
        write_reg!(self, SX1302_REG_AGC_MCU_CTRL_MCU_CLEAR, 0x00);

        let val = read_reg!(self, SX1302_REG_AGC_MCU_CTRL_PARITY_ERROR);
        if val != 0 {
            return Err(Error::from(GatewayError::Radio(format!(
                "Failed to load AGC fw: parity error check failed"
            ))));
        }
        debug!("AGC fw loaded");

        Ok(())
    }

    pub async fn agc_start(&self, expected_version: i32) -> Result<(), Error> {
        const AGC_RADIO_A_INIT_DONE: u8 = 0x80;
        const AGC_RADIO_B_INIT_DONE: u8 = 0x20;

        let ana_gain = SX1302_AGC_RADIO_GAIN_AUTO;
        let dec_gain = SX1302_AGC_RADIO_GAIN_AUTO;

        let fdd_mode = match self.config.full_duplex {
            true => 1,
            false => 0,
        };

        /* Wait for AGC fw to be started, and VERSION available in mailbox */
        self.agc_wait_status(0x01).await?; /* fw has started, VERSION is ready in mailbox */

        let fw_version = self.agc_mailbox_read(0).await?;
        debug!("AGC FW Version: {}", fw_version);
        if fw_version != expected_version {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong AGC fw version ({})",
                fw_version
            ))));
        }

        /* Configure Radio A gains */
        self.agc_mailbox_write(0, ana_gain).await?; /* 0:auto agc*/
        self.agc_mailbox_write(1, dec_gain).await?;
        if self.config.rf_chain_cfg[self.config.clksrc as usize].radio_type != RadioType::SX1250 {
            info!("AGC: setting fdd_mode to {}", fdd_mode);
            self.agc_mailbox_write(2, fdd_mode).await?;
        }

        /* notify AGC that gains has been set to mailbox for Radio A */
        self.agc_mailbox_write(3, AGC_RADIO_A_INIT_DONE).await?;

        /* Wait for AGC to acknowlage it has received gain settings for Radio A */
        self.agc_wait_status(0x02).await?;

        /* Check ana_gain setting */
        if self.agc_mailbox_read(0).await? != ana_gain as i32 {
            return Err(Error::from(GatewayError::Radio(format!(
                "Analog gain of Radio A has not been set properly"
            ))));
        }

        /* Check dec_gain setting */
        if self.agc_mailbox_read(1).await? != dec_gain as i32 {
            return Err(Error::from(GatewayError::Radio(format!(
                "Decimator gain of Radio A has not been set properly"
            ))));
        }

        /* Check FDD mode setting */
        if self.agc_mailbox_read(2).await? != fdd_mode as i32 {
            return Err(Error::from(GatewayError::Radio(format!(
                "FDD mode of Radio A has not been set properly"
            ))));
        }

        debug!("AGC: Radio A config done");

        /* Configure Radio B gains */
        self.agc_mailbox_write(0, ana_gain).await?; /* 0:auto agc*/
        self.agc_mailbox_write(1, dec_gain).await?;
        if self.config.rf_chain_cfg[self.config.clksrc as usize].radio_type != RadioType::SX1250 {
            info!("AGC: setting fdd_mode to {}", fdd_mode);
            self.agc_mailbox_write(2, fdd_mode).await?;
        }

        /* notify AGC that gains has been set to mailbox for Radio B */
        self.agc_mailbox_write(3, AGC_RADIO_B_INIT_DONE).await?;

        /* Wait for AGC to acknowlage it has received gain settings for Radio B */
        self.agc_wait_status(0x03).await?;

        /* Check ana_gain setting */
        if self.agc_mailbox_read(0).await? != ana_gain as i32 {
            return Err(Error::from(GatewayError::Radio(format!(
                "Analog gain of Radio B has not been set properly"
            ))));
        }

        /* Check dec_gain setting */
        if self.agc_mailbox_read(1).await? != dec_gain as i32 {
            return Err(Error::from(GatewayError::Radio(format!(
                "Decimator gain of Radio B has not been set properly"
            ))));
        }

        /* Check FDD mode setting */
        if self.agc_mailbox_read(2).await? != fdd_mode as i32 {
            return Err(Error::from(GatewayError::Radio(format!(
                "FDD mode of Radio B has not been set properly"
            ))));
        }

        debug!("AGC: Radio B config done");

        /* Configure AGC gains */
        let agc_params = match self.config.rf_chain_cfg[self.config.clksrc as usize].radio_type {
            RadioType::SX1250 => unsafe { agc_params_sx1250 },
            _ => unsafe { agc_params_sx125x },
        };

        /* Configure analog gain min/max */
        self.agc_mailbox_write(0, agc_params.ana_min).await?;
        self.agc_mailbox_write(1, agc_params.ana_max).await?;

        /* notify AGC that params have been set to mailbox */
        self.agc_mailbox_write(3, 0x03).await?;

        /* Wait for AGC to acknowlage it has received params */
        self.agc_wait_status(0x04).await?;

        /* Check params */
        let val = self.agc_mailbox_read(0).await?;
        if val as u8 != agc_params.ana_min {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong ana_min (w: {} r:{})",
                agc_params.ana_min, val
            ))));
        }

        let val = self.agc_mailbox_read(1).await?;
        if val as u8 != agc_params.ana_max {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong ana_max (w: {} r:{})",
                agc_params.ana_max, val
            ))));
        }

        debug!("AGC: config of analog gain min/max done");

        /* -----------------------------------------------------------------------*/

        /* Configure analog thresholds */
        self.agc_mailbox_write(0, agc_params.ana_thresh_l).await?;
        self.agc_mailbox_write(1, agc_params.ana_thresh_h).await?;

        /* notify AGC that params have been set to mailbox */
        self.agc_mailbox_write(3, 0x04).await?;

        /* Wait for AGC to acknoledge it has received params */
        self.agc_wait_status(0x05).await?;

        /* Check params */
        let val = self.agc_mailbox_read(0).await?;
        if val as u8 != agc_params.ana_thresh_l {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong ana_thresh_l (w: {} r:{})",
                agc_params.ana_thresh_l, val
            ))));
        }
        let val = self.agc_mailbox_read(1).await?;
        if val as u8 != agc_params.ana_thresh_h {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong ana_thresh_h (w: {} r:{})",
                agc_params.ana_thresh_h, val
            ))));
        }

        debug!("AGC: config of analog threshold done");

        /* -----------------------------------------------------------------------*/

        /* Configure decimator attenuation min/max */
        self.agc_mailbox_write(0, agc_params.dec_attn_min).await?;
        self.agc_mailbox_write(1, agc_params.dec_attn_max).await?;

        /* notify AGC that params have been set to mailbox */
        self.agc_mailbox_write(3, 0x05).await?;

        /* Wait for AGC to acknoledge it has received params */
        self.agc_wait_status(0x06).await?;

        /* Check params */
        let val = self.agc_mailbox_read(0).await?;
        if val as u8 != agc_params.dec_attn_min {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong dec_attn_min (w: {} r:{})",
                agc_params.dec_attn_min, val
            ))));
        }
        let val = self.agc_mailbox_read(1).await?;
        if val as u8 != agc_params.dec_attn_max {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong dec_attn_max (w: {} r:{})",
                agc_params.dec_attn_max, val
            ))));
        }

        debug!("AGC: config of decimator atten min/max done");

        /* -----------------------------------------------------------------------*/

        /* Configure decimator attenuation thresholds */
        self.agc_mailbox_write(0, agc_params.dec_thresh_l).await?;
        self.agc_mailbox_write(1, agc_params.dec_thresh_h1).await?;
        self.agc_mailbox_write(2, agc_params.dec_thresh_h2).await?;

        /* notify AGC that params have been set to mailbox */
        self.agc_mailbox_write(3, 0x06).await?;

        /* Wait for AGC to acknoledge it has received params */
        self.agc_wait_status(0x07).await?;

        /* Check params */
        let val = self.agc_mailbox_read(0).await?;
        if val as u8 != agc_params.dec_thresh_l {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong dec_thresh_l (w: {} r:{})",
                agc_params.dec_thresh_l, val
            ))));
        }
        let val = self.agc_mailbox_read(1).await?;
        if val as u8 != agc_params.dec_thresh_h1 {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong dec_thresh_h1 (w: {} r:{})",
                agc_params.dec_thresh_h1, val
            ))));
        }
        let val = self.agc_mailbox_read(2).await?;
        if val as u8 != agc_params.dec_thresh_h2 {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong dec_thresh_h2 (w: {} r:{})",
                agc_params.dec_thresh_h2, val
            ))));
        }

        debug!("AGC: config of decimator threshold done");

        /* -----------------------------------------------------------------------*/

        /* Configure channel attenuation min/max */
        self.agc_mailbox_write(0, agc_params.chan_attn_min).await?;
        self.agc_mailbox_write(1, agc_params.chan_attn_max).await?;

        /* notify AGC that params have been set to mailbox */
        self.agc_mailbox_write(3, 0x07).await?;

        /* Wait for AGC to acknoledge it has received params */
        self.agc_wait_status(0x08).await?;

        /* Check params */
        let val = self.agc_mailbox_read(0).await?;
        if val as u8 != agc_params.chan_attn_min {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong chan_attn_min (w: {} r:{})",
                agc_params.chan_attn_min, val
            ))));
        }
        let val = self.agc_mailbox_read(1).await?;
        if val as u8 != agc_params.chan_attn_max {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong chan_attn_max (w: {} r:{})",
                agc_params.chan_attn_max, val
            ))));
        }

        debug!("AGC: config of channel atten min/max done");

        /* -----------------------------------------------------------------------*/

        /* Configure channel attenuation threshold */
        self.agc_mailbox_write(0, agc_params.chan_thresh_l).await?;
        self.agc_mailbox_write(1, agc_params.chan_thresh_h).await?;

        /* notify AGC that params have been set to mailbox */
        self.agc_mailbox_write(3, 0x08).await?;

        /* Wait for AGC to acknoledge it has received params */
        self.agc_wait_status(0x09).await?;

        /* Check params */
        let val = self.agc_mailbox_read(0).await?;
        if val as u8 != agc_params.chan_thresh_l {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong chan_thresh_l (w: {} r:{})",
                agc_params.chan_thresh_l, val
            ))));
        }
        let val = self.agc_mailbox_read(1).await?;
        if val as u8 != agc_params.chan_thresh_h {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong chan_thresh_h (w: {} r:{})",
                agc_params.chan_attn_max, val
            ))));
        }

        debug!("AGC: config of channel atten threshold done");

        /* -----------------------------------------------------------------------*/

        /* Configure sx1250 SetPAConfig */
        if self.config.rf_chain_cfg[self.config.clksrc as usize].radio_type == RadioType::SX1250 {
            self.agc_mailbox_write(0, agc_params.deviceSel).await?;
            self.agc_mailbox_write(1, agc_params.hpMax).await?;
            self.agc_mailbox_write(2, agc_params.paDutyCycle).await?;

            /* notify AGC that params have been set to mailbox */
            self.agc_mailbox_write(3, 0x09).await?;

            /* Wait for AGC to acknoledge it has received params */
            self.agc_wait_status(0x0A).await?;

            /* Check params */
            let val = self.agc_mailbox_read(0).await?;
            if val as u8 != agc_params.deviceSel {
                return Err(Error::from(GatewayError::Radio(format!(
                    "wrong deviceSel (w: {} r:{})",
                    agc_params.deviceSel, val
                ))));
            }
            let val = self.agc_mailbox_read(1).await?;
            if val as u8 != agc_params.hpMax {
                return Err(Error::from(GatewayError::Radio(format!(
                    "wrong hpMax (w: {} r:{})",
                    agc_params.hpMax, val
                ))));
            }
            let val = self.agc_mailbox_read(2).await?;
            if val as u8 != agc_params.paDutyCycle {
                return Err(Error::from(GatewayError::Radio(format!(
                    "wrong paDutyCycle (w: {} r:{})",
                    agc_params.paDutyCycle, val
                ))));
            }

            debug!("AGC: config of sx1250 PA optimal settings done");
        }

        /* -----------------------------------------------------------------------*/

        /* Set PA start delay */
        let pa_start_delay = 8;
        self.agc_mailbox_write(0, pa_start_delay).await?; /* 1 LSB = 100 µs*/

        /* notify AGC that params have been set to mailbox */
        self.agc_mailbox_write(3, 0x0A).await?;

        /* Wait for AGC to acknoledge it has received params */
        self.agc_wait_status(0x0B).await?;

        /* Check params */
        let val = self.agc_mailbox_read(0).await?;
        if val as u8 != pa_start_delay {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong PA start delay (w: {} r:{})",
                pa_start_delay, val
            ))));
        }

        debug!("AGC: config of PA start delay done");

        /* -----------------------------------------------------------------------*/

        /* Enable LBT if required */
        self.agc_mailbox_write(
            0,
            match self.config.lbt_enable {
                true => 1,
                false => 0,
            },
        )
        .await?;

        /* notify AGC that params have been set to mailbox */
        self.agc_mailbox_write(3, 0x0B).await?;

        /* Wait for AGC to acknoledge it has received params */
        self.agc_wait_status(0x0F).await?;

        /* Check params */
        let val = self.agc_mailbox_read(0).await?;
        if val != 0 && !self.config.lbt_enable {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong LBT configuration (w: {} r:{})",
                self.config.lbt_enable, val
            ))));
        }

        debug!(
            "AGC: LBT is {}",
            match self.config.lbt_enable {
                true => "enabled",
                false => "disabled",
            }
        );

        /* -----------------------------------------------------------------------*/

        /* notify AGC that configuration is finished */
        self.agc_mailbox_write(3, 0x0F).await?;

        debug!("AGC: started");

        Ok(())
    }

    async fn agc_wait_status(&self, status: u8) -> Result<(), Error> {
        while read_reg!(self, SX1302_REG_AGC_MCU_MCU_AGC_STATUS_MCU_AGC_STATUS) as u8 != status {
            wait_ms(1).await;
        }

        Ok(())
    }

    async fn agc_mailbox_read(&self, mailbox: u8) -> Result<i32, Error> {
        if mailbox > 3 {
            return Err(Error::from(GatewayError::Config(format!(
                "invalid AGC mailbox ID: {}",
                mailbox
            ))));
        }
        let reg =
            SX1302_REG_AGC_MCU_MCU_MAIL_BOX_RD_DATA_BYTE0_MCU_MAIL_BOX_RD_DATA - mailbox as u32;
        Ok(read_reg!(self, reg))
    }

    async fn agc_mailbox_write(&self, mailbox: u8, value: u8) -> Result<(), Error> {
        if mailbox > 3 {
            return Err(Error::from(GatewayError::Config(format!(
                "invalid AGC mailbox ID: {}",
                mailbox
            ))));
        }
        let reg =
            SX1302_REG_AGC_MCU_MCU_MAIL_BOX_WR_DATA_BYTE0_MCU_MAIL_BOX_WR_DATA - mailbox as u32;
        Ok(write_reg!(self, reg, value))
    }

    pub fn arb_firmware() -> &'static [u8] {
        unsafe {
            let retval = std::ptr::slice_from_raw_parts(get_arb_firmware(), 8192);
            &*retval
        }
    }

    pub async fn arb_load_firmware(&self, fw: &[u8]) -> Result<(), Error> {
        use memcmp::Memcmp;

        /* Take control over ARB MCU */
        write_reg!(self, SX1302_REG_ARB_MCU_CTRL_MCU_CLEAR, 0x01);
        write_reg!(self, SX1302_REG_ARB_MCU_CTRL_HOST_PROG, 0x01);
        write_reg!(self, SX1302_REG_COMMON_PAGE_PAGE, 0x00);

        /* Write ARB fw in ARB MEM */
        self.interface
            .sx130x_mem_write(ARB_MEM_ADDR, bytes::Bytes::copy_from_slice(fw))
            .await?;

        /* Read back and check */
        let fw_check = self
            .interface
            .sx130x_mem_read(ARB_MEM_ADDR, 8192, false)
            .await?;

        if !fw.memcmp(&fw_check[..]) {
            return Err(Error::from(GatewayError::Radio(format!(
                "AGC fw read/write check failed"
            ))));
        }

        /* Release control over ARB MCU */
        write_reg!(self, SX1302_REG_ARB_MCU_CTRL_HOST_PROG, 0x00);
        write_reg!(self, SX1302_REG_ARB_MCU_CTRL_MCU_CLEAR, 0x00);

        let val = read_reg!(self, SX1302_REG_ARB_MCU_CTRL_PARITY_ERROR);
        if val != 0 {
            return Err(Error::from(GatewayError::Radio(format!(
                "Failed to load ARB fw: parity error check failed"
            ))));
        }
        info!("ARB fw loaded");
        Ok(())
    }

    pub async fn arb_start(&self) -> Result<(), Error> {
        const FW_VERSION_ARB: i32 = 2;
        const DR_LORA_SF7: u8 = 7;

        /* Wait for ARB fw to be started, and VERSION available in debug registers */
        self.arb_wait_status(0x01).await?;

        /* Get firmware VERSION */
        let val = self.arb_debug_read(0).await?;
        debug!("ARB FW VERSION: {}", val);
        if val != FW_VERSION_ARB {
            return Err(Error::from(GatewayError::Radio(format!(
                "wrong ARB fw version ({})",
                val
            ))));
        }

        /* Enable/disable ARB detect/modem alloc stats for the specified SF */
        self.arb_set_debug_stats(true, DR_LORA_SF7).await?;

        /* Enable/Disable double demod for different timing set (best timestamp / best demodulation) - 1 bit per SF (LSB=SF5, MSB=SF12) => 0:Disable 1:Enable */
        if self.config.fine_timestamping == false {
            info!("ARB: dual demodulation disabled for all SF");
            self.arb_debug_write(3, 0x00).await?; /* double demod disabled for all SF */
        } else {
            match self.config.fine_timestamp_mode {
                FineTimestampMode::AllSF => {
                    info!("ARB: dual demodulation enabled for all SF");
                    self.arb_debug_write(3, 0xFF).await?; /* double demod enabled for all SF */
                }
                FineTimestampMode::HighCapacity => {
                    info!("ARB: dual demodulation enabled for SF5 -> SF10");
                    self.arb_debug_write(3, 0x3F).await?; /* double demod enabled for SF10 <- SF5 */
                }
            }
        }

        info!("ARB: dual demodulation disabled for all SF");
        self.arb_debug_write(3, 0x00).await?; /* double demod disabled for all SF */

        /* Set double detect packet filtering threshold [0..3] */
        self.arb_debug_write(2, 3).await?;

        /* Notify ARB that it can resume */
        self.arb_debug_write(1, 1).await?;

        /* Wait for ARB to acknoledge */
        self.arb_wait_status(0x00).await?;

        debug!("ARB: started");

        Ok(())
    }

    async fn arb_wait_status(&self, status: u8) -> Result<(), Error> {
        while read_reg!(self, SX1302_REG_ARB_MCU_MCU_ARB_STATUS_MCU_ARB_STATUS) as u8 != status {
            wait_ms(1).await;
        }

        Ok(())
    }

    async fn arb_debug_read(&self, reg_id: u8) -> Result<i32, Error> {
        if reg_id > 15 {
            return Err(Error::from(GatewayError::Config(format!(
                "invlid ARB debug register: {}",
                reg_id
            ))));
        }

        let reg = SX1302_REG_ARB_MCU_ARB_DEBUG_STS_0_ARB_DEBUG_STS_0 + reg_id as u32;
        Ok(read_reg!(self, reg))
    }

    async fn arb_set_debug_stats(&self, enable: bool, sf: u8) -> Result<(), Error> {
        if enable {
            debug!("ARB: Debug stats enabled for SF{}", sf);
            write_reg!(self, SX1302_REG_ARB_MCU_ARB_DEBUG_CFG_0_ARB_DEBUG_CFG_0, sf);
        } else {
            debug!("ARB: Debug stats disabled");
        }
        Ok(())
    }

    async fn arb_debug_write(&self, reg_id: u8, value: u8) -> Result<(), Error> {
        if reg_id > 15 {
            return Err(Error::from(GatewayError::Config(format!(
                "invlid ARB debug register: {}",
                reg_id
            ))));
        }

        let reg = SX1302_REG_ARB_MCU_ARB_DEBUG_CFG_0_ARB_DEBUG_CFG_0 + reg_id as u32;
        Ok(write_reg!(self, reg, value))
    }

    pub async fn tx_configure(&self) -> Result<(), Error> {
        /* Select the TX destination interface */
        match self.config.rf_chain_cfg[self.config.clksrc as usize].radio_type {
            RadioType::SX1250 => {
                /* Let AGC control PLL DIV (sx1250 only) */
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_A_TX_RFFE_IF_CTRL2_PLL_DIV_CTRL_AGC,
                    1
                );
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_B_TX_RFFE_IF_CTRL2_PLL_DIV_CTRL_AGC,
                    1
                );

                /* SX126x Tx RFFE */
                write_reg!(self, SX1302_REG_TX_TOP_A_TX_RFFE_IF_CTRL_TX_IF_DST, 0x01);
                write_reg!(self, SX1302_REG_TX_TOP_B_TX_RFFE_IF_CTRL_TX_IF_DST, 0x01);
            }
            RadioType::SX1255 | RadioType::SX1257 => {
                /* SX1255/57 Tx RFFE */
                write_reg!(self, SX1302_REG_TX_TOP_A_TX_RFFE_IF_CTRL_TX_IF_DST, 0x00);
                write_reg!(self, SX1302_REG_TX_TOP_B_TX_RFFE_IF_CTRL_TX_IF_DST, 0x00);
            }
            o => {
                return Err(Error::from(GatewayError::Config(format!(
                    "radio type: {:?} not supported",
                    o
                ))));
            }
        }

        /* Configure the TX mode of operation */
        write_reg!(self, SX1302_REG_TX_TOP_A_TX_RFFE_IF_CTRL_TX_MODE, 0x01); /* Modulation */
        write_reg!(self, SX1302_REG_TX_TOP_B_TX_RFFE_IF_CTRL_TX_MODE, 0x01); /* Modulation */

        /* Configure the output data clock edge */
        write_reg!(self, SX1302_REG_TX_TOP_A_TX_RFFE_IF_CTRL_TX_CLK_EDGE, 0x00); /* Data on rising edge */
        write_reg!(self, SX1302_REG_TX_TOP_B_TX_RFFE_IF_CTRL_TX_CLK_EDGE, 0x00); /* Data on rising edge */

        Ok(())
    }

    pub async fn gps_enable(&self, enable: bool) -> Result<(), Error> {
        if enable == true {
            write_reg!(self, SX1302_REG_TIMESTAMP_GPS_CTRL_GPS_EN, 1);
            write_reg!(self, SX1302_REG_TIMESTAMP_GPS_CTRL_GPS_POL, 1);
        /* invert polarity for PPS */
        } else {
            write_reg!(self, SX1302_REG_TIMESTAMP_GPS_CTRL_GPS_EN, 0);
        }
        Ok(())
    }

    pub async fn get_eui(&self) -> Result<MacAddress, Error> {
        let mut eui_data = [0_u8; 8];
        for i in 0..8 {
            write_reg!(self, SX1302_REG_OTP_BYTE_ADDR_ADDR, i);
            let val = read_reg!(self, SX1302_REG_OTP_RD_DATA_RD_DATA);
            eui_data[i] = val as u8;
        }

        Ok(MacAddress::from(eui_data))
    }

    pub async fn read_fifo(&self) -> Result<Option<Bytes>, Error> {
        let reg = reg_info(SX1302_REG_RX_TOP_RX_BUFFER_NB_BYTES_MSB_RX_BUFFER_NB_BYTES);

        /* Check if there is data in the FIFO */
        let v = self.interface.sx130x_mem_read(reg.addr, 2, false).await?;
        let nb_bytes_1 = u16::from_be_bytes([v[0], v[1]]);

        /* Workaround for multi-byte read issue: read again and ensure new read is not lower than the previous one */
        let v = self.interface.sx130x_mem_read(reg.addr, 2, false).await?;
        let nb_bytes_2 = u16::from_be_bytes([v[0], v[1]]);

        let nb_bytes = nb_bytes_1.max(nb_bytes_2);
        if nb_bytes > 0 {
            Ok(Some(
                self.interface
                    .sx130x_mem_read(0x4000, nb_bytes, true)
                    .await?,
            ))
        } else {
            Ok(None)
        }
    }

    pub async fn send(&self, pkt: TxPkt) -> Result<(), Error> {
        info!("start send: {}hz", pkt.freq_hz);

        let modulation = Modulation::Lora;
        let chirp_lowpass;

        if pkt.rf_chain >= 2 {
            return Err(Error::custom(format!(
                "INVALID RF_CHAIN TO SEND PACKETS: {}",
                pkt.rf_chain
            )));
        }

        if self.config.rf_chain_cfg[pkt.rf_chain as usize].enable == false {
            return Err(Error::custom("SELECTED RF_CHAIN IS DISABLED"));
        }

        let tx_gain_lut = self.config.rf_chain_cfg[pkt.rf_chain as usize]
            .tx_gain_lut
            .as_ref()
            .ok_or(Error::custom("missing tx_gain_lut"))?;

        /* Set PA gain with AD5338R when using full duplex CN490 ref design */
        // if (CONTEXT_BOARD.full_duplex == true) {
        //     uint8_t volt_val[AD5338R_CMD_SIZE] = {0x39, VOLTAGE2HEX_H(2.51), VOLTAGE2HEX_L(2.51)}; /* set to 2.51V */
        //     err = ad5338r_write(ad_fd, I2C_PORT_DAC_AD5338R, volt_val);
        //     if (err != LGW_I2C_SUCCESS) {
        //         printf("ERROR: failed to set voltage by ad5338r\n");
        //         return LGW_HAL_ERROR;
        //     }
        //     printf("INFO: AD5338R: Set DAC output to 0x%02X 0x%02X\n", (uint8_t)VOLTAGE2HEX_H(2.51), (uint8_t)VOLTAGE2HEX_L(2.51));
        // }

        /* Start Listen-Before-Talk */
        // if (CONTEXT_SX1261.lbt_conf.enable == true) {
        //     err = lgw_lbt_start(&CONTEXT_SX1261, pkt_data);
        //     if (err != 0) {
        //         printf("ERROR: failed to start LBT\n");
        //         return LGW_HAL_ERROR;
        //     }
        // }

        write_reg!(
            self,
            SX1302_REG_TX_TOP_GEN_CFG_0_MODULATION_TYPE(pkt.rf_chain),
            0x00
        );
        write_reg!(
            self,
            SX1302_REG_TX_TOP_TX_RFFE_IF_CTRL_TX_IF_SRC(pkt.rf_chain),
            0x01
        );

        let mut pow_index = tx_gain_lut.len();
        /* Find the proper index in the TX gain LUT according to requested rf_power */
        while pow_index > 0 {
            pow_index -= 1;
            if tx_gain_lut[pow_index].rf_power <= pkt.rf_power {
                break;
            }
        }

        let tx_lut = &tx_gain_lut[pow_index];

        info!("selecting TX Gain LUT index: {}", pow_index);

        /* loading calibrated Tx DC offsets */
        // err = lgw_reg_w(SX1302_REG_TX_TOP_TX_RFFE_IF_I_OFFSET_I_OFFSET(pkt_data->rf_chain), tx_lut->lut[pow_index].offset_i);
        // CHECK_ERR(err);
        // err = lgw_reg_w(SX1302_REG_TX_TOP_TX_RFFE_IF_Q_OFFSET_Q_OFFSET(pkt_data->rf_chain), tx_lut->lut[pow_index].offset_q);
        // CHECK_ERR(err);

        // DEBUG_PRINTF("INFO: Applying IQ offset (i:%d, q:%d)\n", tx_lut->lut[pow_index].offset_i, tx_lut->lut[pow_index].offset_q);

        let power;
        /* Set the power parameters to be used for TX */
        match self.config.rf_chain_cfg[pkt.rf_chain as usize].radio_type {
            RadioType::SX1250 => {
                let pa_en = if tx_lut.pa_gain > 0 { 1_u8 } else { 0_u8 }; /* only 1 bit used to control the external PA */
                power = (pa_en << 6) | tx_lut.pwr_idx;
            }

            _ => {
                todo!();
            }
        }

        write_reg!(
            self,
            SX1302_REG_TX_TOP_AGC_TX_PWR_AGC_TX_PWR(pkt.rf_chain),
            power
        );

        /* Set digital gain */
        write_reg!(
            self,
            SX1302_REG_TX_TOP_TX_RFFE_IF_IQ_GAIN_IQ_GAIN(pkt.rf_chain),
            tx_lut.dig_gain
        );

        /* Set Tx frequency */
        let freq_reg = match self.config.rf_chain_cfg[pkt.rf_chain as usize].radio_type {
            RadioType::SX1255 => SX1302_FREQ_TO_REG(pkt.freq_hz * 2),

            _ => SX1302_FREQ_TO_REG(pkt.freq_hz),
        };

        write_reg!(
            self,
            SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_RF_H_FREQ_RF(pkt.rf_chain),
            (freq_reg >> 16) & 0xFF
        );
        write_reg!(
            self,
            SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_RF_M_FREQ_RF(pkt.rf_chain),
            (freq_reg >> 8) & 0xFF
        );
        write_reg!(
            self,
            SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_RF_L_FREQ_RF(pkt.rf_chain),
            (freq_reg >> 0) & 0xFF
        );

        /* Set AGC bandwidth and modulation type*/
        let mod_bw = match modulation {
            Modulation::Lora => pkt.datarate.1 as u8,

            e => {
                todo!("support modulation {:?}", e)
            }
        };
        write_reg!(
            self,
            SX1302_REG_TX_TOP_AGC_TX_BW_AGC_TX_BW(pkt.rf_chain),
            mod_bw
        );

        /* Configure modem */
        match modulation {
            Modulation::Lora => {
                /* Set bandwidth */
                let freq_dev = pkt.datarate.1.hz() / 2;
                let fdev_reg = SX1302_FREQ_TO_REG(freq_dev);
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_DEV_H_FREQ_DEV(pkt.rf_chain),
                    (fdev_reg >> 8) & 0xFF
                );
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_DEV_L_FREQ_DEV(pkt.rf_chain),
                    (fdev_reg >> 0) & 0xFF
                );
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG0_0_MODEM_BW(pkt.rf_chain),
                    pkt.datarate.1 as u8
                );

                /* Preamble length */
                let preamble = match pkt.preamble {
                    None => {
                        /* if not explicit, use recommended LoRa preamble size */
                        STD_LORA_PREAMBLE
                    }

                    Some(preamble) if preamble < MIN_LORA_PREAMBLE => {
                        warn!("preamble length adjusted to respect minimum LoRa size");
                        MIN_LORA_PREAMBLE
                    }

                    Some(preamble) => preamble,
                };

                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG1_3_PREAMBLE_SYMB_NB(pkt.rf_chain),
                    (preamble >> 8) & 0xFF
                );
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG1_2_PREAMBLE_SYMB_NB(pkt.rf_chain),
                    (preamble >> 0) & 0xFF
                );

                /* LoRa datarate */
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG0_0_MODEM_SF(pkt.rf_chain),
                    pkt.datarate.0 as u8
                );

                /* Chirp filtering */
                chirp_lowpass = if pkt.datarate.0 < SpreadingFactor::SF10 {
                    6
                } else {
                    7
                };
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TX_CFG0_0_CHIRP_LOWPASS(pkt.rf_chain),
                    chirp_lowpass
                );

                /* Coding Rate */
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG0_1_CODING_RATE(pkt.rf_chain),
                    pkt.coderate as u8
                );

                /* Start LoRa modem */
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG0_2_MODEM_EN(pkt.rf_chain),
                    1
                );
                write_reg!(self, SX1302_REG_TX_TOP_TXRX_CFG0_2_CADRXTX(pkt.rf_chain), 2);
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG1_1_MODEM_START(pkt.rf_chain),
                    1
                );
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TX_CFG0_0_CONTINUOUS(pkt.rf_chain),
                    0
                );

                /* Modulation options */
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TX_CFG0_0_CHIRP_INVERT(pkt.rf_chain),
                    if pkt.invert_pol { 1 } else { 0 }
                );
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG0_2_IMPLICIT_HEADER(pkt.rf_chain),
                    if pkt.no_header { 1 } else { 0 }
                );
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG0_2_CRC_EN(pkt.rf_chain),
                    if pkt.no_crc { 0 } else { 1 }
                );

                /* Syncword */
                if self.config.public == false
                    || pkt.datarate.0 == SpreadingFactor::SF5
                    || pkt.datarate.0 == SpreadingFactor::SF6
                {
                    debug!("Setting LoRa syncword 0x12");
                    write_reg!(
                        self,
                        SX1302_REG_TX_TOP_FRAME_SYNCH_0_PEAK1_POS(pkt.rf_chain),
                        2
                    );
                    write_reg!(
                        self,
                        SX1302_REG_TX_TOP_FRAME_SYNCH_1_PEAK2_POS(pkt.rf_chain),
                        4
                    );
                } else {
                    debug!("Setting LoRa syncword 0x34");
                    write_reg!(
                        self,
                        SX1302_REG_TX_TOP_FRAME_SYNCH_0_PEAK1_POS(pkt.rf_chain),
                        6
                    );
                    write_reg!(
                        self,
                        SX1302_REG_TX_TOP_FRAME_SYNCH_1_PEAK2_POS(pkt.rf_chain),
                        8
                    );
                }

                /* Set Fine Sync for SF5/SF6 */
                if pkt.datarate.0 == SpreadingFactor::SF5 || pkt.datarate.0 == SpreadingFactor::SF6
                {
                    debug!("Enable Fine Sync");
                    write_reg!(
                        self,
                        SX1302_REG_TX_TOP_TXRX_CFG0_2_FINE_SYNCH_EN(pkt.rf_chain),
                        1
                    );
                } else {
                    write_reg!(
                        self,
                        SX1302_REG_TX_TOP_TXRX_CFG0_2_FINE_SYNCH_EN(pkt.rf_chain),
                        0
                    );
                }

                /* Set Payload length */
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG0_3_PAYLOAD_LENGTH(pkt.rf_chain),
                    pkt.payload.len()
                );

                /* Set PPM offset (low datarate optimization) */
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TXRX_CFG0_1_PPM_OFFSET_HDR_CTRL(pkt.rf_chain),
                    0
                );
                if SET_PPM_ON(pkt.datarate.1, pkt.datarate.0) {
                    debug!("Low datarate optimization ENABLED");
                    write_reg!(
                        self,
                        SX1302_REG_TX_TOP_TXRX_CFG0_1_PPM_OFFSET(pkt.rf_chain),
                        1
                    );
                } else {
                    write_reg!(
                        self,
                        SX1302_REG_TX_TOP_TXRX_CFG0_1_PPM_OFFSET(pkt.rf_chain),
                        0
                    );
                }
            }

            e => {
                todo!("support modulation {:?}", e)
            }
        }

        /* Set TX start delay */
        self.tx_set_start_delay(
            &pkt,
            self.config.rf_chain_cfg[pkt.rf_chain as usize].radio_type,
            pkt.datarate,
            chirp_lowpass,
        )
        .await?;

        /* Write payload in transmit buffer */
        write_reg!(
            self,
            SX1302_REG_TX_TOP_TX_CTRL_WRITE_BUFFER(pkt.rf_chain),
            0x01
        );
        let mem_addr = REG_SELECT(pkt.rf_chain as usize, 0x5300, 0x5500) as u16;
        if modulation == Modulation::FSK {
            todo!("support FSK");
        } else {
            self.interface
                .sx130x_mem_write(mem_addr, pkt.payload)
                .await?;
        }
        write_reg!(
            self,
            SX1302_REG_TX_TOP_TX_CTRL_WRITE_BUFFER(pkt.rf_chain),
            0x00
        );

        /* Trigger transmit */
        debug!("Start Tx: Freq:{}, {}", pkt.freq_hz, pkt.datarate);
        match pkt.tx_mode {
            TxMode::Immediate => {
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TX_TRIG_TX_TRIG_IMMEDIATE(pkt.rf_chain),
                    0x00
                ); // reset state machine
                write_reg!(
                    self,
                    SX1302_REG_TX_TOP_TX_TRIG_TX_TRIG_IMMEDIATE(pkt.rf_chain),
                    0x01
                );
            }

            e => todo!("support tx_mode: {:?}", e),
        }

        info!("end send");

        Ok(())
    }

    async fn tx_set_start_delay(
        &self,
        pkt: &TxPkt,
        radio_type: RadioType,
        datarate: Datarate,
        chirp_lowpass: u8,
    ) -> Result<u16, Error> {
        let radio_bw_delay = match radio_type {
            RadioType::SX1250 => match datarate.1 {
                Bandwidth::BW125 => 19,
                Bandwidth::BW250 => 24,
                Bandwidth::BW500 => 21,
            },

            _ => {
                todo!("support other radio types");
            }
        };

        /* Adjust with modulation */
        let filter_delay: u16 =
            (((1 << chirp_lowpass) - 1) as f32 * 1e6 / datarate.1.hz() as f32) as u16;
        let modem_delay: u16 = (8.0 * (32e6 / (32 * datarate.1.hz()) as f32)) as u16; /* if bw=125k then modem freq=4MHz */

        /* Compute total delay */
        let mut tx_start_delay: u16 = TX_START_DELAY_DEFAULT * 32;
        tx_start_delay -= radio_bw_delay + filter_delay + modem_delay;

        debug!(
            "tx_start_delay={} (filter_delay={} modem_delay={})",
            tx_start_delay, filter_delay, modem_delay
        );
        let data = [
            ((tx_start_delay >> 8) & 0xFF) as u8,
            ((tx_start_delay >> 0) & 0xFF) as u8,
        ];

        let reg = reg_info(SX1302_REG_TX_TOP_TX_START_DELAY_MSB_TX_START_DELAY(
            pkt.rf_chain,
        ));
        self.interface
            .sx130x_mem_write(reg.addr, bytes::Bytes::copy_from_slice(&data))
            .await?;
        Ok(tx_start_delay)
    }
}

const STD_LORA_PREAMBLE: u16 = 8;
const MIN_LORA_PREAMBLE: u16 = 6;
const TX_START_DELAY_DEFAULT: u16 = 1500; // Calibrated value for 500KHz BW

fn reg_info(reg: u32) -> lgw_reg_s {
    unsafe {
        let mut r = MaybeUninit::zeroed().assume_init();
        loragw_reg_info(reg, &mut r);
        r
    }
}

#[allow(non_snake_case)]
fn REG_SELECT(i: usize, a: u32, b: u32) -> u32 {
    if i == 0 {
        a
    } else {
        b
    }
}

#[inline]
#[allow(non_snake_case)]
fn IF_HZ_TO_REG(f: i32) -> i32 {
    (f * 32) / 15625
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_GEN_CFG_0_MODULATION_TYPE(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_GEN_CFG_0_MODULATION_TYPE
    } else {
        SX1302_REG_TX_TOP_B_GEN_CFG_0_MODULATION_TYPE
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_RFFE_IF_CTRL_TX_IF_SRC(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_RFFE_IF_CTRL_TX_IF_SRC
    } else {
        SX1302_REG_TX_TOP_B_TX_RFFE_IF_CTRL_TX_IF_SRC
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_AGC_TX_PWR_AGC_TX_PWR(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_AGC_TX_PWR_AGC_TX_PWR
    } else {
        SX1302_REG_TX_TOP_B_AGC_TX_PWR_AGC_TX_PWR
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_RFFE_IF_IQ_GAIN_IQ_GAIN(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_RFFE_IF_IQ_GAIN_IQ_GAIN
    } else {
        SX1302_REG_TX_TOP_B_TX_RFFE_IF_IQ_GAIN_IQ_GAIN
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_FREQ_TO_REG(freq_hz: u32) -> u32 {
    ((freq_hz as u64 * (1 << 18)) / 32_000_000) as u32
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_RF_H_FREQ_RF(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_RFFE_IF_FREQ_RF_H_FREQ_RF
    } else {
        SX1302_REG_TX_TOP_B_TX_RFFE_IF_FREQ_RF_H_FREQ_RF
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_RF_M_FREQ_RF(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_RFFE_IF_FREQ_RF_M_FREQ_RF
    } else {
        SX1302_REG_TX_TOP_B_TX_RFFE_IF_FREQ_RF_M_FREQ_RF
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_RF_L_FREQ_RF(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_RFFE_IF_FREQ_RF_L_FREQ_RF
    } else {
        SX1302_REG_TX_TOP_B_TX_RFFE_IF_FREQ_RF_L_FREQ_RF
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_AGC_TX_BW_AGC_TX_BW(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_AGC_TX_BW_AGC_TX_BW
    } else {
        SX1302_REG_TX_TOP_B_AGC_TX_BW_AGC_TX_BW
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_DEV_H_FREQ_DEV(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_RFFE_IF_FREQ_DEV_H_FREQ_DEV
    } else {
        SX1302_REG_TX_TOP_B_TX_RFFE_IF_FREQ_DEV_H_FREQ_DEV
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_RFFE_IF_FREQ_DEV_L_FREQ_DEV(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_RFFE_IF_FREQ_DEV_L_FREQ_DEV
    } else {
        SX1302_REG_TX_TOP_B_TX_RFFE_IF_FREQ_DEV_L_FREQ_DEV
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_0_MODEM_BW(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_0_MODEM_BW
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_0_MODEM_BW
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG1_3_PREAMBLE_SYMB_NB(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG1_3_PREAMBLE_SYMB_NB
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG1_3_PREAMBLE_SYMB_NB
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG1_2_PREAMBLE_SYMB_NB(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG1_2_PREAMBLE_SYMB_NB
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG1_2_PREAMBLE_SYMB_NB
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_0_MODEM_SF(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_0_MODEM_SF
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_0_MODEM_SF
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_CFG0_0_CHIRP_LOWPASS(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_CFG0_0_CHIRP_LOWPASS
    } else {
        SX1302_REG_TX_TOP_B_TX_CFG0_0_CHIRP_LOWPASS
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_1_CODING_RATE(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_1_CODING_RATE
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_1_CODING_RATE
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_2_MODEM_EN(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_2_MODEM_EN
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_2_MODEM_EN
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_2_CADRXTX(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_2_CADRXTX
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_2_CADRXTX
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG1_1_MODEM_START(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG1_1_MODEM_START
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG1_1_MODEM_START
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_CFG0_0_CONTINUOUS(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_CFG0_0_CONTINUOUS
    } else {
        SX1302_REG_TX_TOP_B_TX_CFG0_0_CONTINUOUS
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_CFG0_0_CHIRP_INVERT(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_CFG0_0_CHIRP_INVERT
    } else {
        SX1302_REG_TX_TOP_B_TX_CFG0_0_CHIRP_INVERT
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_2_IMPLICIT_HEADER(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_2_IMPLICIT_HEADER
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_2_IMPLICIT_HEADER
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_2_CRC_EN(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_2_CRC_EN
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_2_CRC_EN
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_FRAME_SYNCH_0_PEAK1_POS(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_FRAME_SYNCH_0_PEAK1_POS
    } else {
        SX1302_REG_TX_TOP_B_FRAME_SYNCH_0_PEAK1_POS
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_FRAME_SYNCH_1_PEAK2_POS(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_FRAME_SYNCH_1_PEAK2_POS
    } else {
        SX1302_REG_TX_TOP_B_FRAME_SYNCH_1_PEAK2_POS
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_2_FINE_SYNCH_EN(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_2_FINE_SYNCH_EN
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_2_FINE_SYNCH_EN
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_3_PAYLOAD_LENGTH(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_3_PAYLOAD_LENGTH
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_3_PAYLOAD_LENGTH
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_1_PPM_OFFSET_HDR_CTRL(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_1_PPM_OFFSET_HDR_CTRL
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_1_PPM_OFFSET_HDR_CTRL
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TXRX_CFG0_1_PPM_OFFSET(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TXRX_CFG0_1_PPM_OFFSET
    } else {
        SX1302_REG_TX_TOP_B_TXRX_CFG0_1_PPM_OFFSET
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_START_DELAY_MSB_TX_START_DELAY(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_START_DELAY_MSB_TX_START_DELAY
    } else {
        SX1302_REG_TX_TOP_B_TX_START_DELAY_MSB_TX_START_DELAY
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_CTRL_WRITE_BUFFER(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_CTRL_WRITE_BUFFER
    } else {
        SX1302_REG_TX_TOP_B_TX_CTRL_WRITE_BUFFER
    }
}

#[inline]
#[allow(non_snake_case)]
fn SX1302_REG_TX_TOP_TX_TRIG_TX_TRIG_IMMEDIATE(rf_chain: u8) -> u32 {
    if rf_chain == 0 {
        SX1302_REG_TX_TOP_A_TX_TRIG_TX_TRIG_IMMEDIATE
    } else {
        SX1302_REG_TX_TOP_B_TX_TRIG_TX_TRIG_IMMEDIATE
    }
}

async fn wait_ms(ms: u64) {
    tokio::time::sleep(Duration::from_millis(ms)).await;
}

fn calculate_freq_to_time_drift(freq_hz: u32, bw: Bandwidth, mant: &mut u16, exp: &mut u8) {
    *exp = 0;
    let mut mantissa_u64 = bw.hz() as u64 * (2 << (20 - 1)) / freq_hz as u64;
    while mantissa_u64 < 2048 {
        *exp += 1;
        mantissa_u64 <<= 1;
    }

    *mant = mantissa_u64 as u16;
}

#[allow(non_snake_case)]
fn SET_PPM_ON(bw: Bandwidth, dr: SpreadingFactor) -> bool {
    ((bw == Bandwidth::BW125) && ((dr == SpreadingFactor::SF11) || (dr == SpreadingFactor::SF12)))
        || ((bw == Bandwidth::BW250) && (dr == SpreadingFactor::SF12))
}
