use std::net::SocketAddr;

use crate::{base64_serde, cfg::RootConfig};
use bytes::{BufMut, Bytes, BytesMut};
use log::{debug, error, info, warn};
use loragw_hw::lib::{nlighten, MacAddress};
use serde::Serialize;
use tokio::net::UdpSocket;

use super::types::{PullResp, RxType};

#[derive(Debug, Serialize)]
struct PushPacket {
    pub rxpk: Vec<push::RxPk>,
}

#[derive(Debug, Serialize)]
struct PushPacketSig {
    pub key: u32,
    #[serde(with = "base64_serde")]
    pub sig: bytes::Bytes,
}

impl From<nlighten::types::RxPktSig> for PushPacketSig {
    fn from(pkt: nlighten::types::RxPktSig) -> Self {
        Self {
            key: pkt.id,
            sig: bytes::Bytes::copy_from_slice(pkt.sig.as_slice()),
        }
    }
}

mod push {
    use loragw_hw::lib::{nlighten, Datarate, GPSTime, WGS84Position};
    use serde::Serialize;
    use serde_repr::Serialize_repr;

    #[derive(Debug, Serialize)]
    pub struct RxPk {
        pub key: u32,
        pub pos: Option<WGS84Position>,
        pub snr: i16,
        pub freq: u32,
        pub rssis: i16,
        pub rssic: u8,
        pub tmst: u32,
        pub datr: Datarate,
        pub gps_time: Option<GPSTime>,
        #[serde(with = "super::base64_serde")]
        pub data: bytes::Bytes,
        pub stat: CRC,
    }

    impl From<nlighten::types::FullRxPkt> for RxPk {
        fn from(pkt: nlighten::types::FullRxPkt) -> Self {
            Self {
                key: pkt.id,
                pos: pkt.pkt.pos,
                snr: pkt.pkt.snr,
                freq: pkt.pkt.freq,
                rssis: pkt.pkt.rssi,
                rssic: pkt.rssic,
                tmst: pkt.pkt.tmst,
                datr: pkt.pkt.datarate,
                gps_time: pkt.pkt.gps_time,
                data: bytes::Bytes::copy_from_slice(pkt.pkt.payload.as_slice()),
                stat: if pkt.crc_en {
                    if pkt.crc_err {
                        CRC::Fail
                    } else {
                        CRC::OK
                    }
                } else {
                    CRC::Disabled
                },
            }
        }
    }

    #[derive(Debug, Serialize_repr, Clone, PartialEq)]
    #[repr(i8)]
    pub enum CRC {
        Disabled = 0,
        OK = 1,
        Fail = -1,
    }
}

pub struct GWMPClient {
    socket: UdpSocket,
    gateway_mac: MacAddress,
}

impl GWMPClient {
    pub async fn new(root_cfg: &RootConfig, gateway_mac: MacAddress) -> anyhow::Result<Self> {
        info!("GWMP Client bind: {}", &root_cfg.gateway_conf.local_address);
        let socket = UdpSocket::bind(&root_cfg.gateway_conf.local_address).await?;

        info!(
            "GWMP Client connect: {}",
            &root_cfg.gateway_conf.server_address
        );
        socket
            .connect(&root_cfg.gateway_conf.server_address)
            .await?;
        Ok(Self {
            socket: socket,
            gateway_mac: gateway_mac,
        })
    }

    pub async fn recv(&self) -> anyhow::Result<(SocketAddr, RxType)> {
        let mut buf = [0_u8; 50000];
        let (len, addr) = self.socket.recv_from(&mut buf).await?;

        debug!("rx from: {}", addr);

        let _proto_version = buf[0];
        let _token = u16::from_be_bytes([buf[1], buf[2]]);
        let type_id = u8::from_be_bytes([buf[3]]);

        match type_id {
            0x03 => {
                // PULL_RESP
                let value: PullResp = serde_json::from_slice(&buf[4..(len - 4)])?;
                Ok((addr, RxType::PullResp(value)))
            }

            0x04 => {
                // PULL_ACK
                Ok((addr, RxType::PullAck))
            }

            _ => Err(anyhow::anyhow!("unknown type_id: {}", type_id)),
        }
    }

    pub async fn send_push(&self, pkt: nlighten::types::SCStreamMsg) {
        info!("sending {:?}", pkt);
        match pkt {
            nlighten::types::SCStreamMsg::RxPkt(pkt) => {
                assert_eq!(self.gateway_mac, pkt.pkt.card_id);
                let token = rand::random();
                match create_push_data(token, &self.gateway_mac, pkt) {
                    Ok(buf) => {
                        if let Err(e) = self.socket.send(&buf).await {
                            error!("error sending udp: {}", e);
                        }
                    }
                    Err(e) => error!("{}", e),
                }
            }
            nlighten::types::SCStreamMsg::RxPktSig(sig) => {
                let token = rand::random();
                match create_push_data_sig(token, &self.gateway_mac, sig) {
                    Ok(buf) => {
                        if let Err(e) = self.socket.send(&buf).await {
                            error!("error sending udp: {}", e);
                        }
                    }
                    Err(e) => error!("{}", e),
                }
            }

            other => {
                warn!("unhandled msg: {:?}", other);
            }
        }
    }

    pub async fn send_keep_alive(&self) {
        let token = rand::random();
        match create_keep_alive(token, &self.gateway_mac) {
            Ok(buf) => {
                if let Err(e) = self.socket.send(&buf).await {
                    error!("error sending keep alive: {}", e);
                }
            }
            Err(e) => error!("{}", e),
        }
    }
}

fn create_push_data(
    token: u16,
    gateway_id: &MacAddress,
    packet: nlighten::types::FullRxPkt,
) -> Result<Bytes, serde_json::Error> {
    let mut buf = BytesMut::with_capacity(1024);
    buf.put_u8(3); // protocol version
    buf.put_u16(token);
    buf.put_u8(0x00); // PUSH_DATA id
    buf.put(gateway_id.as_bytes());

    let data = PushPacket {
        rxpk: vec![packet.into()],
    };

    let mut serializer = serde_json::Serializer::new(buf.writer());
    data.serialize(&mut serializer)?;
    Ok(serializer.into_inner().into_inner().freeze())
}

fn create_push_data_sig(
    token: u16,
    gateway_id: &MacAddress,
    packet: nlighten::types::RxPktSig,
) -> Result<Bytes, serde_json::Error> {
    let mut buf = BytesMut::with_capacity(1024);
    buf.put_u8(3); // protocol version
    buf.put_u16(token);
    buf.put_u8(0x06); // PUSH_DATA_SIG id
    buf.put(gateway_id.as_bytes());

    let data = PushPacketSig::from(packet);

    let mut serializer = serde_json::Serializer::new(buf.writer());
    data.serialize(&mut serializer)?;
    Ok(serializer.into_inner().into_inner().freeze())
}

fn create_keep_alive(token: u16, gateway_id: &MacAddress) -> Result<Bytes, serde_json::Error> {
    let mut buf = BytesMut::with_capacity(16);
    buf.put_u8(3); // protocol version
    buf.put_u16(token);
    buf.put_u8(0x02); // PULL_DATA id
    buf.put(gateway_id.as_bytes());
    Ok(buf.freeze())
}
