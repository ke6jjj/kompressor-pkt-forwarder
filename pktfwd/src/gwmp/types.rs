use std::collections::HashMap;

use loragw_hw::lib::Datarate;
use serde::{Deserialize, Serialize};

use crate::base64_serde;

pub enum RxType {
    PullResp(PullResp),
    PullAck,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PullResp {
    pub txpk: TxPk,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TxPk {
    pub imme: bool,

    pub tmst: Option<u32>,
    pub tmms: Option<u32>,
    pub freq: f32,
    pub rfch: u32,
    pub powe: Option<i8>,
    pub modu: String,
    pub datr: Datarate,
    pub codr: String,
    #[serde(default)]
    pub ipol: bool,
    pub prea: Option<u16>,
    pub size: u32,
    #[serde(with = "base64_serde")]
    pub data: Vec<u8>,

    #[serde(default)]
    pub ncrc: bool,

    #[serde(default)]
    pub nhdr: bool,

    #[serde(flatten)]
    pub other: HashMap<String, serde_json::Value>,
}
