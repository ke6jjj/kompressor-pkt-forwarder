//mod cfg;
mod protocol;

use anyhow::{Context, Result};
use clap::Parser;
use log::{debug, error, info, warn};
use std::{
    fs::File,
    io::BufReader,
    ops::Deref,
    path::PathBuf,
    thread::sleep,
    time::{self, Duration},
};
use sysfs_gpio::{Direction, Pin};

//use libloragw_sx1302_sys::*;
use loragw_hw::*;

//use futures::prelude::*;
//use futures::stream::FuturesUnordered;

use tokio::signal::unix::{signal, SignalKind};
use tokio_stream::StreamExt;

use cfg::{GatewayConfig, RootConfig};

use crate::protocol::PktFwdClient;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(short, long, parse(from_os_str), value_name = "FILE")]
    config: PathBuf,
}

fn init_logging() {
    use simple_logger::SimpleLogger;
    SimpleLogger::new().init().unwrap();
}

fn reset() -> sysfs_gpio::Result<()> {
    debug!("resetting hardware");
    let sx130x_reset_pin = Pin::new(17);
    sx130x_reset_pin.with_exported(|| {
        sleep(time::Duration::from_millis(200));
        sx130x_reset_pin.set_direction(Direction::Out)?;
        sx130x_reset_pin.set_value(1)?;
        sleep(time::Duration::from_millis(200));
        sx130x_reset_pin.set_value(0)?;
        sleep(time::Duration::from_millis(200));
        Ok(())
    })
}

#[tokio::main]
async fn main() -> Result<()> {
    init_logging();

    let cli = Cli::parse();

    let cfg_file = File::open(cli.config.deref())?;
    let reader = json_comments::StripComments::new(cfg_file);
    let root_config: RootConfig = serde_json::from_reader(reader).with_context(|| {
        format!(
            "error parsing config file: {}",
            cli.config.as_path().display()
        )
    })?;

    info!("Starting pktfwd");

    let com: Result<Box<SecureConcentrator<_,_>>, _> = match root_config.board.com_type.as_str() {
        "SPI_PROXY" => {
            Ok(Box::new(loragw_hw::with_spi(root_config.board.com_path.as_str())?))
        }
        t => Err(GatewayError::Config(format!("unknown com type: {}", t))),
    };

    let mut gateway = LoraGateway {
        secure_concentrator: com?,
        reset_pin: root_config.board.reset_pin,
        eui: None,
    };
    gateway.start()?;

    let mut pktfwd_client =
        PktFwdClient::new(&root_config, gateway.eui.expect("gateway eui")).await?;

    let mut sigterm = signal(SignalKind::terminate())?;
    let mut sigint = signal(SignalKind::interrupt())?;
    //let mut term_handler : FuturesUnordered<_> = vec![sigterm.recv(), sigint.recv()].into_iter().collect();
    let mut keep_alive = tokio::time::interval(time::Duration::from_secs(
        root_config.gateway_conf.keep_alive.into(),
    ));

    let pkt_stream = gateway.into_stream();
    tokio::pin!(pkt_stream);

    loop {
        tokio::select! {

            // r = gateway.receive_pkt() => {
            //     match r {
            //         Ok(None) => {},
            //         Ok(Some(pkt)) => {
            //             info!("pkt received");
            //             info!("pkt: {}", hex::encode(&pkt.buf[..]));
            //         },
            //         Err(e) => {
            //             warn!("wtf: {}", e);
            //         }
            //     }
            // },

            Some(pkt) = pkt_stream.next() => {
                info!("pkt received");

                // if let RxPktType::RxPkt(ref pkt) = pkt {
                //     let gps_time = pkt.pkt.gps_time.unwrap();
                //     info!("pkt gps time: {}", gps_time);
                // }
                //info!("pkt: {}", hex::encode(&pkt.buf[..]));
                pktfwd_client.send_push(pkt).await;
            },

            _ = keep_alive.tick() => {
                pktfwd_client.send_keep_alive().await;
            }


            _ = sigterm.recv() => {
                info!("Shutting down due to SIGTERM.");
                break;
            },

            _ = sigint.recv() => {
                info!("Shutting down due to SIGINT ");
                break;
            },

        }
    }

    info!("shutting down");

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;
}
